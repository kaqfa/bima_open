# BIMA OPEN

Website Biro Mahasiswa Udinus

## Installation

Create virtual environtment

```bash
  python -m venv venv
```

Load virtual environtment (Windows)

```bash
    venv\Scripts\activate
```

Load virtual environtment (Linux/MacOS)

```bash
    source myenv/bin/activate
```

Copy .env (Windows)

```bash
    copy .env-example .env
```

Copy .env (Linux/MacOS)

```bash
    cp .env-example .env
```

Install requirements

```bash
    pip install -r requirements.txt
```

Migrate Database

```bash
    python -m manage migrate
```

Load Fixture

```bash
    python -m manage loaddata BIMA/fixture/group.json
    python -m manage loaddata BIMA/fixture/group-permission.json
    python -m manage loaddata BIMA/fixture/ormawa-level.json
    python -m manage loaddata BIMA/fixture/ormawa-jenis-kegiatan.json
```
