FROM python:3.11


ENV LC_ALL=id_ID.UTF-8
ENV LC_CTYPE=id_ID.UTF-8

RUN apt-get update && apt-get install -y locales

RUN echo "id_ID.UTF-8 UTF-8" > /etc/locale.gen && \
    locale-gen

WORKDIR /app/BIMA

COPY requirements.txt /app/BIMA

RUN pip install --no-cache-dir -r requirements.txt

COPY . /app/BIMA

EXPOSE 8000

CMD ["python", "manage.py", "runserver", "0.0.0.0:8000"]