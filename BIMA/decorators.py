from django.contrib.auth.decorators import user_passes_test

def is_member_of_group(group_name):
    def test(user):
        return user.is_authenticated and user.groups.filter(name=group_name).exists()
    return test

def group_required(group_name):
    def decorator(view_func):
        decorated_view_func = user_passes_test(is_member_of_group(group_name))
        return decorated_view_func(view_func)
    return decorator
