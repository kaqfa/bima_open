from django.contrib import messages
from django.shortcuts import redirect


def is_admin_kemahasiswaan(user):
    return True if user.groups.filter(id=1).exists() else False

def is_admin_ormawa(user):
    return True if user.groups.filter(id=2).exists() else False


def form_success_redirect(request, message, route):
    messages.success(request, message)
    return redirect(route)

def form_error_redirect(request, form, route):
    for field, errors in form.errors.items():
        for error in errors:
            messages.error(request, f"[Error] {field}: {error}")
    return redirect(route)