from django import template

register = template.Library()

@register.filter(name="split_last")
def split_last(value, key):
  return value.split(key)[-1]