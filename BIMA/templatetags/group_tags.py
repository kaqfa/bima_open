
from django import template

register = template.Library()

@register.filter(name='has_group') 
def has_group(user, group_name):
    return user.groups.filter(name=group_name).exists() 

@register.filter(name='is_admin_kemahasiswaan') 
def is_admin_kemahasiswaan(user):
    return True if user.groups.filter(id=1).exists() else False

@register.filter(name='is_admin_ormawa') 
def is_admin_ormawa(user):
    return True if user.groups.filter(id=2).exists() else False