from django import template

register = template.Library()

@register.filter(name='format_number')
def format_number(value):
    try:
        value = float(value)
        return "{:,.0f}".format(value)
    except ValueError:
        return value