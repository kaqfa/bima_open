from allauth.socialaccount.signals import pre_social_login
from allauth.account.signals import user_logged_in
from allauth.exceptions import ImmediateHttpResponse

from django.db.models import Q
from django.dispatch import receiver
from django.contrib.auth.models import User
from django.contrib import messages
from django.shortcuts import redirect

from mahasiswa.models import Mahasiswa
from ormawa.models import Kepengurusan
from datetime import date


@receiver(pre_social_login)
def social_login_user_check(request, sociallogin, **kwargs):
    address, domain = sociallogin.user.email.split('@')

    # validate email domain=
    if domain == "mhs.dinus.ac.id":
        # convert nim by addres of email
        nim = str(chr(64 + int(address[0]))) + address[1:3] + '.' + address[3:7] + '.' + address[7:]

        # check is nim on mahasiswa list
        # and check kepengurusan is valid
        mhsFound = Mahasiswa.objects.filter(nim=nim).first()
        if not mhsFound:
            messages.warning(request, f'User is not Allowed.')
            raise ImmediateHttpResponse(redirect('login'))

        kepengurusanFound = Kepengurusan.objects.filter(Q(ketua=mhsFound) | Q(sekretaris=mhsFound) & Q(periode__tgl_selesai__gt=date.today().strftime("%Y-%m-%d")))
        if not kepengurusanFound:
            messages.warning(request, f'User is Not Allowed.')
            raise ImmediateHttpResponse(redirect('login'))
            
        
        userFound = User.objects.filter(username=nim).first()
        if userFound:
            sociallogin.user = userFound
            return

        # change username to nim
        sociallogin.user.username = nim
    else:
        messages.warning(request, f'User is Not Allowed.')
        raise ImmediateHttpResponse(redirect('login'))



@receiver(user_logged_in)
def redirect_user_after_login(request, user, **kwargs):
    return redirect('dashboard')