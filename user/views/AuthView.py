from django.shortcuts import render, redirect
from django.contrib.auth.models import User, Group
from django.contrib import auth, messages
from django.db.models import Q

from mahasiswa.models import Mahasiswa
from ormawa.models import Kepengurusan

from dotenv import load_dotenv

from user.forms import LoginForm
load_dotenv()  
from datetime import date
import requests
import os


class AuthView:
    def login(request):
        if request.method == "POST":
            
            form = LoginForm(request.POST)
            
            if not form.is_valid():
                print(form.error)
            
            else:
                username = form.cleaned_data['username']
                password = form.cleaned_data['password']

                # If username is not contain patterns mahasiswa
                # it will login with django
                if not len(username.split('.')) == 3:
                    user = auth.authenticate(request, username=username, password=password)

                # If username likes "xxx.xxxx.xxxxx" (contains 3 dots) will detected as mahasiswa
                # and if mahasiswa it will login with api
                # and it will store jwt token as password (before store it django will encrypt it too)
                # and if mahasiswa login again, their password will updated again / password will dynamic
                else:
                    # check if mahasiswa has valid kepengurusan
                    mhsFound = Mahasiswa.objects.filter(nim=username).first()
                    if not mhsFound:
                        messages.warning(request, f'User is not Allowed.')
                        return redirect('login')

                    kepengurusanFound = Kepengurusan.objects.filter(Q(ketua=mhsFound) | Q(sekretaris=mhsFound) & Q(periode__tgl_selesai__gt=date.today().strftime("%Y-%m-%d")))
                    print(Kepengurusan.objects.filter(ketua=mhsFound))
                    if not kepengurusanFound:
                        messages.warning(request, f'User is Not Allowed.')
                        return redirect('login')

                    # Auth with api
                    # When response code is not 200 user will be back to login page
                    r = requests.post(
                        url=os.getenv('LOGIN_API_URL'),
                        json={
                            "username": username,
                            "password": password,
                        }
                    )
                    if r.status_code != 200:
                        messages.warning(request, f'Invalid Username or Password.')
                        return redirect('login')

                        
                    body = r.json()

                    # Check user is found or not, and if not found create it
                    # When user is found update password
                    group = Group.objects.get(name="ADMIN ORMAWA")
                    user = User.objects.filter(username=username).first()
                    if user == None:
                        user = User.objects.create_user(username, "", body['data']['refresh_token'])

                        # add group and connect mahasiswa to user
                        user.groups.add(group)
                        mhsFound.user = user
                        mhsFound.save()

                    else:
                        # add group and connect mahasiswa to user
                        user.groups.add(group)
                        mhsFound.user = user
                        mhsFound.save()

                        user.set_password(body['data']['refresh_token'])
                        user.save()
                    
                    user = auth.authenticate(request, username=user.username, password=body['data']['refresh_token'])

                if user is not None:
                    auth.login(request, user)
                    return redirect('dashboard')
                else:
                    messages.warning(request, f'Invalid Username or Password.')
                    return redirect('login')
 
        else:
            if request.user.is_authenticated:
                return redirect('dashboard')

            form = LoginForm()
            context = {
                'form': form,
            }
            return render(request, 'auth/login.html', context)

    
    def logout(request):
        auth.logout(request)

        return redirect('login')
