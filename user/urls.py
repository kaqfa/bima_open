from django.urls import path
from . import views
from django.contrib.auth import views as auth_views

urlpatterns = [
    path("login", views.AuthView.login, name="login"),
    path("logout", views.AuthView.logout, name="logout"),
]