from django.contrib import admin

from ormawa.models import JenisKegiatan
from proposalLPJ.models import Anggaran, Panitia, Seksi

# Register your models here.

@admin.register(JenisKegiatan)
class JenisKegiatanAdmin(admin.ModelAdmin):
    list_display = [
        'id',
        'nama_jenis'
    ]

@admin.register(Anggaran)
class AnggaranAdmin(admin.ModelAdmin):
    list_display = [
            'id',
            'seksi', 
            'qty', 
            'harga', 
            'satuan', 
            'real_qty',
            'real_harga',
            'kegiatan',
            'tambahan',
        ]  
    

@admin.register(Panitia)
class PanitiaAdmin(admin.ModelAdmin):
    list_display = [
        'mahasiswa',
        'seksi',
        'koordinator'
    ]

@admin.register(Seksi)
class SeksiAdmin(admin.ModelAdmin):
    list_display = [
        'nama'
    ]