from django.contrib.auth.decorators import login_required
from django.urls import path

from proposalLPJ import views


    # Proposal
urlpatterns = [
    path("proposal", login_required(views.ProposalView.index), name="proposal.index"),
    path("proposal/show/<int:kegiatanID>", login_required(views.ProposalView.show), name="proposal.show"),
    path("proposal/show/<int:kegiatanID>/submit", login_required(views.ProposalView.user_submit), name="proposal.submit"),
    path("proposal/show/<int:kegiatanID>/verify", login_required(views.ProposalView.admin_verify), name="proposal.verify"),
    path("proposal/show/<int:kegiatanID>/print", login_required(views.ProposalView.print), name="proposal.print"),


    # pengantar
    path("proposal/show/<int:kegiatanID>/pengantar/edit", login_required(views.PengantarView.edit), name="pengantar.edit"),

    # acara
    path("proposal/show/<int:kegiatanID>/acara/create", login_required(views.AcaraView.create), name="acara.create"),
    path("proposal/show/<int:kegiatanID>/acara/edit/<int:acaraID>", login_required(views.AcaraView.edit), name="acara.edit"),
    path("proposal/show/<int:kegiatanID>/acara/delete/<int:acaraID>", login_required(views.AcaraView.delete), name="acara.delete"),

    # Panitia
    path("proposal/show/<int:kegiatanID>/panitia/create", login_required(views.PanitiaView.create), name="panitia.create"),
    path("proposal/show/<int:kegiatanID>/panitia/edit/<int:panitiaID>", login_required(views.PanitiaView.edit), name="panitia.edit"),
    path("proposal/show/<int:kegiatanID>/panitia/delete/<int:panitiaID>", login_required(views.PanitiaView.delete), name="panitia.delete"),

    # Seksi
    path("proposal/show/<int:kegiatanID>/seksi/create", login_required(views.SeksiView.create), name="seksi.create"),
    path("proposal/show/<int:kegiatanID>/seksi/edit/<int:seksiID>", login_required(views.SeksiView.edit), name="seksi.edit"),
    path("proposal/show/<int:kegiatanID>/seksi/delete/<int:seksiID>", login_required(views.SeksiView.delete), name="seksi.delete"),

    # Anggaran
    path("proposal/show/<int:kegiatanID>/anggaran/create", login_required(views.AnggaranView.create), name="anggaran.create"),
    path("proposal/show/<int:kegiatanID>/anggaran/edit/<int:anggaranID>", login_required(views.AnggaranView.edit), name="anggaran.edit"),
    path("proposal/show/<int:kegiatanID>/anggaran/delete/<int:anggaranID>", login_required(views.AnggaranView.delete), name="anggaran.delete"),

    # Pemasukan
    path("proposal/show/<int:kegiatanID>/pemasukan/create", login_required(views.PemasukanView.create), name="pemasukan.create"),
    path("proposal/show/<int:kegiatanID>/pemasukan/edit/<int:pemasukanID>", login_required(views.PemasukanView.edit), name="pemasukan.edit"),
    path("proposal/show/<int:kegiatanID>/pemasukan/delete/<int:pemasukanID>", login_required(views.PemasukanView.delete), name="pemasukan.delete"),

    # print
]


# LPJ 
urlpatterns += [
    path("lpj", login_required(views.LPJView.index), name="lpj.index"),
    path("lpj/show/<int:kegiatanID>", login_required(views.LPJView.show), name="lpj.show"),
    path("lpj/show/<int:kegiatanID>/submit", login_required(views.LPJView.submit), name="lpj.submit"),
    path("lpj/show/<int:kegiatanID>/verify", login_required(views.LPJView.admin_verify), name="lpj.verify"),
    path("lpj/show/<int:kegiatanID>/print", login_required(views.LPJView.print), name="lpj.print"),


    # Hasil
    path("lpj/show/<int:kegiatanID>/hasil/edit", login_required(views.HasilView.edit), name="hasil.edit"),

    # Realisasi Acara
    path("lpj/show/<int:kegiatanID>/realisasi-acara/realisasi/<int:acaraID>", login_required(views.RealisasiAcaraView.edit), name="acara.realisasi"),
    path("lpj/show/<int:kegiatanID>/realisasi-acara/tambahan", login_required(views.RealisasiAcaraView.create), name="acara.tambahan"),

    # Realisasi Pengeluaran
    path("lpj/show/<int:kegiatanID>/realisasi-anggaran/realisasi/<int:anggaranID>", login_required(views.RealisasiPengeluaranView.edit), name="anggaran.realisasi"),
    path("lpj/show/<int:kegiatanID>/realisasi-anggaran/tambahan", login_required(views.RealisasiPengeluaranView.create), name="anggaran.tambahan"),

    # Realisasi Pemasukan
    path("lpj/show/<int:kegiatanID>/pemasukan/realisasi/<int:pemasukanID>", login_required(views.RealisasiDanaAkademikView.edit), name="pemasukan.realisasi"),
    path("lpj/show/<int:kegiatanID>/pemasukan/tambahan", login_required(views.RealisasiDanaAkademikView.create), name="pemasukan.tambahan"),

    # Bukti Pengeluaran
    path("lpj/show/<int:kegiatanID>/bukti-pengeluaran/create", login_required(views.BuktiPengeluaranView.create), name="buktiPengeluaran.create"),
    path("lpj/show/<int:kegiatanID>/bukti-pengeluaran/edit/<int:pemasukanID>", login_required(views.BuktiPengeluaranView.edit), name="buktiPengeluaran.edit"),
    path("lpj/show/<int:kegiatanID>/bukti-pengeluaran/delete/<int:pemasukanID>", login_required(views.BuktiPengeluaranView.delete), name="buktiPengeluaran.delete"),

    # Dokumentasi
    path("lpj/show/<int:kegiatanID>/dokumentasi/create", login_required(views.DokumentasiView.create), name="dokumentasi.create"),
    path("lpj/show/<int:kegiatanID>/dokumentasi/edit/<int:dokumentasiID>", login_required(views.DokumentasiView.edit), name="dokumentasi.edit"),
    path("lpj/show/<int:kegiatanID>/dokumentasi/delete/<int:dokumentasiID>", login_required(views.DokumentasiView.delete), name="dokumentasi.delete"),
]
