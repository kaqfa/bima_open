from django.db import models

from mahasiswa.models import Mahasiswa
from ormawa.models import Kegiatan

class Dokumentasi(models.Model):
    id = models.AutoField(primary_key=True)
    keterangan = models.CharField(max_length=255)
    file_foto = models.ImageField()
    kegiatan = models.ForeignKey(Kegiatan, on_delete=models.CASCADE, related_name='dokumentasi')

    class Meta:
        verbose_name_plural = "Dokumentasi"

    def __str__(self):
        return self.keterangan


class Seksi(models.Model):
    id = models.AutoField(primary_key=True)
    nama = models.CharField(max_length=255)
    kegiatan = models.ForeignKey(Kegiatan, on_delete=models.CASCADE, related_name='seksi')

    class Meta:
        verbose_name_plural = "Seksi"

    def __str__(self):
        return self.nama


class Panitia(models.Model):
    id = models.AutoField(primary_key=True)
    mahasiswa = models.ForeignKey(Mahasiswa, on_delete=models.CASCADE)
    seksi = models.ForeignKey(Seksi, on_delete=models.CASCADE, related_name='panitia')
    koordinator = models.BooleanField()

    class Meta:
        verbose_name_plural = "Seksi"


class Acara(models.Model):
    id = models.AutoField(primary_key=True)
    nama = models.CharField(max_length=255)
    waktu_mulai = models.DateTimeField()
    waktu_selesai = models.DateTimeField()
    real_waktu_mulai = models.DateTimeField(null=True)
    real_waktu_selesai = models.DateTimeField(null=True)
    kegiatan = models.ForeignKey(Kegiatan, on_delete=models.CASCADE, related_name='acara')
    seksi = models.ForeignKey(Seksi, on_delete=models.CASCADE, related_name='acara')
    tambahan = models.BooleanField(default=False)

    class Meta:
        verbose_name_plural = "Acara"


class Anggaran(models.Model):
    id = models.AutoField(primary_key=True)
    seksi = models.ForeignKey(Seksi, on_delete=models.CASCADE, related_name='anggaran')
    nama = models.CharField(max_length=255)
    qty = models.IntegerField()
    harga = models.IntegerField()
    satuan = models.CharField(max_length=20)
    real_qty = models.IntegerField(null=True)
    real_harga = models.IntegerField(null=True)
    kegiatan = models.ForeignKey(Kegiatan, on_delete=models.CASCADE, related_name='anggaran')
    tambahan = models.BooleanField(default=False)

    @property
    def total(self):
        return self.qty * self.harga
    
    @property
    def total_real(self):
        return self.real_qty * self.real_harga

    class Meta:
        verbose_name_plural = "Anggaran"
    
    def __str__(self):
        return self.nama


class Pemasukan(models.Model):
    id = models.AutoField(primary_key=True)
    kegiatan = models.ForeignKey(Kegiatan, on_delete=models.CASCADE, related_name='pemasukan')
    sumber_dana = models.CharField(max_length=200)
    total_dana = models.IntegerField()
    real_total_dana = models.IntegerField(null=True)
    tambahan = models.BooleanField(default=False)

    class Meta:
        verbose_name_plural = "Pemasukan"


class BuktiPengeluaran(models.Model):
    id = models.AutoField(primary_key=True)
    kegiatan = models.ForeignKey(Kegiatan, on_delete=models.CASCADE, related_name='bukti_pengeluaran')
    file_foto = models.ImageField()
    keterangan = models.CharField(max_length=200)

    class Meta:
        verbose_name_plural = "Bukti Pengeluaran"
