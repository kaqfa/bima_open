from django import forms

from ormawa.models import Kegiatan
from proposalLPJ.models import Acara, Anggaran, BuktiPengeluaran, Dokumentasi, Panitia, Pemasukan, Seksi


class AcaraForm(forms.ModelForm):
    class Meta:
        model = Acara
        fields = ['nama', 'waktu_mulai', 'waktu_selesai', 'seksi']

        widgets = {
            'seksi': forms.Select(
                attrs={
                  'x-model':'acaraEdit.seksi',
                  'class':'w-full rounded-lg border-[1.5px] border-stroke bg-transparent px-5 py-3 font-normal text-black outline-none transition focus:border-primary active:border-primary disabled:cursor-default disabled:bg-whiter dark:border-form-strokedark dark:bg-form-input dark:text-white dark:focus:border-primary',
                },
            ),
        }

    nama = forms.CharField(
        widget=forms.TextInput(
            attrs={
                'x-model':'acaraEdit.nama',
                'placeholder':'Nama Acara',
                'class':'w-full rounded-lg border-[1.5px] border-stroke bg-transparent px-5 py-3 font-normal text-black outline-none transition focus:border-primary active:border-primary disabled:cursor-default disabled:bg-whiter dark:border-form-strokedark dark:bg-form-input dark:text-white dark:focus:border-primary',
            }
        )
    )
    waktu_mulai = forms.DateTimeField(
        widget=forms.DateTimeInput(
            attrs={
                'x-model':'acaraEdit.waktu_mulai',
                'class':'w-full rounded-lg border-[1.5px] border-stroke bg-transparent px-5 py-3 font-normal text-black outline-none transition focus:border-primary active:border-primary disabled:cursor-default disabled:bg-whiter dark:border-form-strokedark dark:bg-form-input dark:text-white dark:focus:border-primary',
                'type': 'datetime-local',
            }
        )
    )
    waktu_selesai = forms.DateTimeField(
        widget=forms.DateTimeInput(
            attrs={
                'x-model':'acaraEdit.waktu_selesai',
                'class':'w-full rounded-lg border-[1.5px] border-stroke bg-transparent px-5 py-3 font-normal text-black outline-none transition focus:border-primary active:border-primary disabled:cursor-default disabled:bg-whiter dark:border-form-strokedark dark:bg-form-input dark:text-white dark:focus:border-primary',
                'type': 'datetime-local',
            }
        )
    )   

class SeksiForm(forms.ModelForm):
    class Meta:
        model = Seksi
        fields = ['nama']

    nama = forms.CharField(
        widget=forms.TextInput(
            attrs={
                'x-model':'seksiEdit.nama',
                'placeholder':'Nama Seksi',
                'class':'w-full rounded-lg border-[1.5px] border-stroke bg-transparent px-5 py-3 font-normal text-black outline-none transition focus:border-primary active:border-primary disabled:cursor-default disabled:bg-whiter dark:border-form-strokedark dark:bg-form-input dark:text-white dark:focus:border-primary',
            }
        )
    )

class PanitiaForm(forms.ModelForm):
    class Meta:
        model = Panitia
        fields = ['seksi', 'koordinator']

        widgets = {
            'seksi': forms.Select(
                attrs={
                  'x-model':'panitiaEdit.seksi',
                  'class':'w-full rounded-lg border-[1.5px] border-stroke bg-transparent px-5 py-3 font-normal text-black outline-none transition focus:border-primary active:border-primary disabled:cursor-default disabled:bg-whiter dark:border-form-strokedark dark:bg-form-input dark:text-white dark:focus:border-primary',
                },
            ),
            'koordinator': forms.CheckboxInput(
                attrs={
                  'value': 'koordinator',
                  'x-model':'panitiaEdit.koordinator',
                  '@change': 'switcherToggle = !switcherToggle; panitiaEdit.koordinator = switcherToggle ? "koordinator" : false',
                  'class':'sr-only',
                  'id': 'toggle3',
                },
            ),
        }
    
    mahasiswa = forms.CharField(
        widget=forms.TextInput(
            attrs={
                'x-model':'panitiaEdit.mahasiswa',
                '@input': '''liveSearch('mahasiswa', 'mahasiswa')''',
                'placeholder':'NIM Mahasiswa',
                'class':'w-full rounded-lg border-[1.5px] border-stroke bg-transparent px-5 py-3 font-normal text-black outline-none transition focus:border-primary active:border-primary disabled:cursor-default disabled:bg-whiter dark:border-form-strokedark dark:bg-form-input dark:text-white dark:focus:border-primary',
            }
        )
    )

class PengantarForm(forms.ModelForm):
    class Meta:
        model = Kegiatan
        fields = [
            'maksud_tujuan', 
            'latar_belakang',
            'dasar',
            'penutup',
            ]

    maksud_tujuan = forms.CharField(
        label="Maksud Tujuan",
        widget=forms.Textarea(
            attrs={
                'x-model':'pengantarEdit.maksud_tujuan',
                '@input': '''autoSave('maksud_tujuan')''',
                'class':'w-full rounded-lg border-[1.5px] border-stroke bg-transparent px-5 py-3 font-normal text-black outline-none transition focus:border-primary active:border-primary disabled:cursor-default disabled:bg-whiter dark:border-form-strokedark dark:bg-form-input dark:text-white dark:focus:border-primary',
            }
        )
    )
    latar_belakang = forms.CharField(
        label="Latar Belakang",
        widget=forms.Textarea(
            attrs={
                'x-model':'pengantarEdit.latar_belakang',
                '@input': '''autoSave('latar_belakang')''',
                'class':'w-full rounded-lg border-[1.5px] border-stroke bg-transparent px-5 py-3 font-normal text-black outline-none transition focus:border-primary active:border-primary disabled:cursor-default disabled:bg-whiter dark:border-form-strokedark dark:bg-form-input dark:text-white dark:focus:border-primary',
            }
        )
    )
    dasar = forms.CharField(
        label="Dasar",
        widget=forms.Textarea(
            attrs={
                'x-model':'pengantarEdit.dasar',
                '@input': '''autoSave('dasar')''',
                'class':'w-full rounded-lg border-[1.5px] border-stroke bg-transparent px-5 py-3 font-normal text-black outline-none transition focus:border-primary active:border-primary disabled:cursor-default disabled:bg-whiter dark:border-form-strokedark dark:bg-form-input dark:text-white dark:focus:border-primary',
            }
        )
    )
    penutup = forms.CharField(
        label="Penutup",
        widget=forms.Textarea(
            attrs={
                'x-model':'pengantarEdit.penutup',
                '@input': '''autoSave('penutup')''',
                'class':'w-full rounded-lg border-[1.5px] border-stroke bg-transparent px-5 py-3 font-normal text-black outline-none transition focus:border-primary active:border-primary disabled:cursor-default disabled:bg-whiter dark:border-form-strokedark dark:bg-form-input dark:text-white dark:focus:border-primary',
            }
        )
    )

class AnggaranForm(forms.ModelForm):
    class Meta:
        model = Anggaran
        fields = [
            'seksi', 
            'nama',
            'qty',
            'harga',
            'satuan',
            ]
        labels = {
            'seksi': 'Seksi Penanggung Jawab',
        }
        widgets = {
            'seksi': forms.Select(
                attrs={
                  'x-model':'anggaranEdit.seksi',
                  'class':'w-full rounded-lg border-[1.5px] border-stroke bg-transparent px-5 py-3 font-normal text-black outline-none transition focus:border-primary active:border-primary disabled:cursor-default disabled:bg-whiter dark:border-form-strokedark dark:bg-form-input dark:text-white dark:focus:border-primary',
                },
            ),
        }

    nama = forms.CharField(
        label="Nama",
        widget=forms.TextInput(
            attrs={
                'x-model':'anggaranEdit.nama',
                'class':'w-full rounded-lg border-[1.5px] border-stroke bg-transparent px-5 py-3 font-normal text-black outline-none transition focus:border-primary active:border-primary disabled:cursor-default disabled:bg-whiter dark:border-form-strokedark dark:bg-form-input dark:text-white dark:focus:border-primary',
            }
        )
    )
    qty = forms.IntegerField(
        label="Qty",
        widget=forms.NumberInput(
            attrs={
                'x-model':'anggaranEdit.qty',
                'class':'w-full rounded-lg border-[1.5px] border-stroke bg-transparent px-5 py-3 font-normal text-black outline-none transition focus:border-primary active:border-primary disabled:cursor-default disabled:bg-whiter dark:border-form-strokedark dark:bg-form-input dark:text-white dark:focus:border-primary',
            }
        )
    )
    harga = forms.IntegerField(
        label="Harga",
        widget=forms.NumberInput(
            attrs={
                'x-model':'anggaranEdit.harga',
                'class':'relative z-20 w-full appearance-none rounded border border-stroke bg-transparent px-12 py-3 outline-none transition focus:border-primary active:border-primary dark:border-form-strokedark dark:bg-form-input text-black dark:text-white',
            }
        )
    )
    satuan = forms.CharField(
        label="Satuan",
        widget=forms.TextInput(
            attrs={
                'x-model':'anggaranEdit.satuan',
                'class':'w-full rounded-lg border-[1.5px] border-stroke bg-transparent px-5 py-3 font-normal text-black outline-none transition focus:border-primary active:border-primary disabled:cursor-default disabled:bg-whiter dark:border-form-strokedark dark:bg-form-input dark:text-white dark:focus:border-primary',
            }
        )
    )

class PemasukanForm(forms.ModelForm):
    class Meta:
        model = Pemasukan
        fields = [
            'sumber_dana', 
            'total_dana',
            ]
        
    sumber_dana = forms.CharField(
        label="Sumber Dana",
        widget=forms.TextInput(
            attrs={
                'x-model':'pemasukanEdit.sumber_dana',
                'class':'w-full rounded-lg border-[1.5px] border-stroke bg-transparent px-5 py-3 font-normal text-black outline-none transition focus:border-primary active:border-primary disabled:cursor-default disabled:bg-whiter dark:border-form-strokedark dark:bg-form-input dark:text-white dark:focus:border-primary',
            }
        )
    )
    total_dana = forms.CharField(
        label="Total Dana",
        widget=forms.TextInput(
            attrs={
                'x-model':'pemasukanEdit.total_dana',
                'class':'w-full rounded-lg border-[1.5px] border-stroke bg-transparent px-5 py-3 font-normal text-black outline-none transition focus:border-primary active:border-primary disabled:cursor-default disabled:bg-whiter dark:border-form-strokedark dark:bg-form-input dark:text-white dark:focus:border-primary',
            }
        )
    )


class HasilForm(forms.ModelForm):
    class Meta:
        model = Kegiatan
        fields = [
            'hasil',
            ]

    hasil = forms.CharField(
        label="Hasil",
        widget=forms.Textarea(
            attrs={
                'x-model':'hasilEdit.hasil',
                '@input': '''autoSave('maksud_tujuan')''',
                'class':'w-full rounded-lg border-[1.5px] border-stroke bg-transparent px-5 py-3 font-normal text-black outline-none transition focus:border-primary active:border-primary disabled:cursor-default disabled:bg-whiter dark:border-form-strokedark dark:bg-form-input dark:text-white dark:focus:border-primary',
            }
        )
    )

class RealisasiAcaraForm(forms.ModelForm):
    class Meta:
        model =  Acara
        fields = [
            'real_waktu_mulai',
            'real_waktu_selesai',
        ]
    real_waktu_mulai = forms.DateTimeField(
        label="Realisasi Waktu Mulai",
        widget=forms.DateTimeInput(
            attrs={
                'x-model':'realisasiAcaraEdit.real_waktu_mulai',
                'class':'w-full rounded-lg border-[1.5px] border-stroke bg-transparent px-5 py-3 font-normal text-black outline-none transition focus:border-primary active:border-primary disabled:cursor-default disabled:bg-whiter dark:border-form-strokedark dark:bg-form-input dark:text-white dark:focus:border-primary',
                'type': 'datetime-local',
            }
        )
    )
    real_waktu_selesai = forms.DateTimeField(
        label="Realisasi Waktu Selesai",
        widget=forms.DateTimeInput(
            attrs={
                'x-model':'realisasiAcaraEdit.real_waktu_selesai',
                'class':'w-full rounded-lg border-[1.5px] border-stroke bg-transparent px-5 py-3 font-normal text-black outline-none transition focus:border-primary active:border-primary disabled:cursor-default disabled:bg-whiter dark:border-form-strokedark dark:bg-form-input dark:text-white dark:focus:border-primary',
                'type': 'datetime-local',
            }
        )
    )

class RealisasiAcaraTambahanForm(forms.ModelForm):
    class Meta:
        model =  Acara
        fields = [
            'nama',
            'real_waktu_mulai',
            'real_waktu_selesai',
            'seksi',
        ]
        widgets = {
            'seksi': forms.Select(
                attrs={
                  'x-model':'acaraEdit.seksi',
                  'class':'w-full rounded-lg border-[1.5px] border-stroke bg-transparent px-5 py-3 font-normal text-black outline-none transition focus:border-primary active:border-primary disabled:cursor-default disabled:bg-whiter dark:border-form-strokedark dark:bg-form-input dark:text-white dark:focus:border-primary',
                },
            ),
        }
    nama = forms.CharField(
        widget=forms.TextInput(
            attrs={
                'x-model':'acaraEdit.nama',
                'placeholder':'Nama Acara',
                'class':'w-full rounded-lg border-[1.5px] border-stroke bg-transparent px-5 py-3 font-normal text-black outline-none transition focus:border-primary active:border-primary disabled:cursor-default disabled:bg-whiter dark:border-form-strokedark dark:bg-form-input dark:text-white dark:focus:border-primary',
            }
        )
    )
    real_waktu_mulai = forms.DateTimeField(
        label="Realisasi Waktu Mulai",
        widget=forms.DateTimeInput(
            attrs={
                'x-model':'realisasiAcaraEdit.real_waktu_mulai',
                'class':'w-full rounded-lg border-[1.5px] border-stroke bg-transparent px-5 py-3 font-normal text-black outline-none transition focus:border-primary active:border-primary disabled:cursor-default disabled:bg-whiter dark:border-form-strokedark dark:bg-form-input dark:text-white dark:focus:border-primary',
                'type': 'datetime-local',
            }
        )
    )
    real_waktu_selesai = forms.DateTimeField(
        label="Realisasi Waktu Selesai",
        widget=forms.DateTimeInput(
            attrs={
                'x-model':'realisasiAcaraEdit.real_waktu_selesai',
                'class':'w-full rounded-lg border-[1.5px] border-stroke bg-transparent px-5 py-3 font-normal text-black outline-none transition focus:border-primary active:border-primary disabled:cursor-default disabled:bg-whiter dark:border-form-strokedark dark:bg-form-input dark:text-white dark:focus:border-primary',
                'type': 'datetime-local',
            }
        )
    )


class RealisasiPengeluaranForm(forms.ModelForm):
    class Meta:
        model = Anggaran
        fields = [
            'real_qty',
            'real_harga',
        ]
    real_qty = forms.IntegerField(
        label="Realisasi Qty",
        widget=forms.NumberInput(
            attrs={
                'x-model':'realisasiPengeluaranEdit.real_qty',
                'class':'w-full rounded-lg border-[1.5px] border-stroke bg-transparent px-5 py-3 font-normal text-black outline-none transition focus:border-primary active:border-primary disabled:cursor-default disabled:bg-whiter dark:border-form-strokedark dark:bg-form-input dark:text-white dark:focus:border-primary',
            }
        )
    )
    real_harga = forms.IntegerField(
        label="Realisasi Harga",
        widget=forms.NumberInput(
            attrs={
                'x-model':'realisasiPengeluaranEdit.real_harga',
                'class':'w-full rounded-lg border-[1.5px] border-stroke bg-transparent px-5 py-3 font-normal text-black outline-none transition focus:border-primary active:border-primary disabled:cursor-default disabled:bg-whiter dark:border-form-strokedark dark:bg-form-input dark:text-white dark:focus:border-primary',
            }
        )
    )
class RealisasiPengeluaranTambahanForm(forms.ModelForm):
    class Meta:
        model = Anggaran
        fields = [
            'seksi', 
            'nama',
            'real_qty',
            'real_harga',
            'satuan',
        ]
        labels = {
            'seksi': 'Seksi Penanggung Jawab',
        }
        widgets = {
            'seksi': forms.Select(
                attrs={
                  'x-model':'anggaranEdit.seksi',
                  'class':'w-full rounded-lg border-[1.5px] border-stroke bg-transparent px-5 py-3 font-normal text-black outline-none transition focus:border-primary active:border-primary disabled:cursor-default disabled:bg-whiter dark:border-form-strokedark dark:bg-form-input dark:text-white dark:focus:border-primary',
                },
            ),
        }

    nama = forms.CharField(
        label="Nama",
        widget=forms.TextInput(
            attrs={
                'x-model':'anggaranEdit.nama',
                'class':'w-full rounded-lg border-[1.5px] border-stroke bg-transparent px-5 py-3 font-normal text-black outline-none transition focus:border-primary active:border-primary disabled:cursor-default disabled:bg-whiter dark:border-form-strokedark dark:bg-form-input dark:text-white dark:focus:border-primary',
            }
        )
    )
    real_qty = forms.IntegerField(
        label="Realisasi Qty",
        widget=forms.NumberInput(
            attrs={
                'x-model':'realisasiPengeluaranEdit.real_qty',
                'class':'w-full rounded-lg border-[1.5px] border-stroke bg-transparent px-5 py-3 font-normal text-black outline-none transition focus:border-primary active:border-primary disabled:cursor-default disabled:bg-whiter dark:border-form-strokedark dark:bg-form-input dark:text-white dark:focus:border-primary',
            }
        )
    )
    real_harga = forms.IntegerField(
        label="Realisasi Harga",
        widget=forms.NumberInput(
            attrs={
                'x-model':'realisasiPengeluaranEdit.real_harga',
                'class':'w-full rounded-lg border-[1.5px] border-stroke bg-transparent px-5 py-3 font-normal text-black outline-none transition focus:border-primary active:border-primary disabled:cursor-default disabled:bg-whiter dark:border-form-strokedark dark:bg-form-input dark:text-white dark:focus:border-primary',
            }
        )
    )
    satuan = forms.CharField(
        label="Satuan",
        widget=forms.TextInput(
            attrs={
                'x-model':'anggaranEdit.satuan',
                'class':'w-full rounded-lg border-[1.5px] border-stroke bg-transparent px-5 py-3 font-normal text-black outline-none transition focus:border-primary active:border-primary disabled:cursor-default disabled:bg-whiter dark:border-form-strokedark dark:bg-form-input dark:text-white dark:focus:border-primary',
            }
        )
    )

class RealisasiDanaAkademikForm(forms.ModelForm):
    class Meta:
        model = Pemasukan
        fields = [
            'real_total_dana',
        ]
    
    real_total_dana =  forms.IntegerField(
        label="Realisasi Total Dana",
        widget=forms.NumberInput(
            attrs={
                'x-model':'realisasiDanaAkademikEdit.real_total_dana',
                'class':'w-full rounded-lg border-[1.5px] border-stroke bg-transparent px-5 py-3 font-normal text-black outline-none transition focus:border-primary active:border-primary disabled:cursor-default disabled:bg-whiter dark:border-form-strokedark dark:bg-form-input dark:text-white dark:focus:border-primary',
            
            }
        )
    )

class RealisasiDanaAkademikTambahanForm(forms.ModelForm):
    class Meta:
        model = Pemasukan
        fields = [
            'sumber_dana', 
            'real_total_dana',
            ]
        
    sumber_dana = forms.CharField(
        label="Sumber Dana",
        widget=forms.TextInput(
            attrs={
                'x-model':'pemasukanEdit.sumber_dana',
                'class':'w-full rounded-lg border-[1.5px] border-stroke bg-transparent px-5 py-3 font-normal text-black outline-none transition focus:border-primary active:border-primary disabled:cursor-default disabled:bg-whiter dark:border-form-strokedark dark:bg-form-input dark:text-white dark:focus:border-primary',
            }
        )
    )
    real_total_dana =  forms.IntegerField(
        label="Realisasi Total Dana",
        widget=forms.NumberInput(
            attrs={
                'x-model':'realisasiDanaAkademikEdit.real_total_dana',
                'class':'w-full rounded-lg border-[1.5px] border-stroke bg-transparent px-5 py-3 font-normal text-black outline-none transition focus:border-primary active:border-primary disabled:cursor-default disabled:bg-whiter dark:border-form-strokedark dark:bg-form-input dark:text-white dark:focus:border-primary',
            
            }
        )
    )

class BuktiPengeluaranForm(forms.ModelForm):
    class Meta:
        model = BuktiPengeluaran
        fields = [
            "file_foto",
            "keterangan",
        ]
    file_foto = forms.ImageField(
        label="File Foto",
        widget=forms.FileInput(
            attrs={
            'x-model':'buktiPengeluaran.file_foto',
            'class':'w-full rounded-lg border-[1.5px] border-stroke bg-transparent px-5 py-3 font-normal text-black outline-none transition focus:border-primary active:border-primary disabled:cursor-default disabled:bg-whiter dark:border-form-strokedark dark:bg-form-input dark:text-white dark:focus:border-primary',
            }
        )
    )
    keterangan = forms.CharField(
        label="Keterangan",
        widget=forms.TextInput(
            attrs={
            'x-model':'buktiPengeluaran.keterangan',
            'class':'w-full rounded-lg border-[1.5px] border-stroke bg-transparent px-5 py-3 font-normal text-black outline-none transition focus:border-primary active:border-primary disabled:cursor-default disabled:bg-whiter dark:border-form-strokedark dark:bg-form-input dark:text-white dark:focus:border-primary',
            }
        )
    )
    
    
    
class DokumentasiForm(forms.ModelForm):
    class Meta:
        model =  Dokumentasi
        fields = [
            "file_foto",
            "keterangan",
        ]
    file_foto = forms.ImageField(
        label="File Foto",
        widget=forms.FileInput(
            attrs={
            'x-model':'dokumentasiEdit.file_foto',
            'class':'w-full rounded-lg border-[1.5px] border-stroke bg-transparent px-5 py-3 font-normal text-black outline-none transition focus:border-primary active:border-primary disabled:cursor-default disabled:bg-whiter dark:border-form-strokedark dark:bg-form-input dark:text-white dark:focus:border-primary',
            }
        )
    )
    keterangan = forms.CharField(
        label="Keterangan",
        widget=forms.TextInput(
            attrs={
            'x-model':'dokumentasiEdit.keterangan',
            'class':'w-full rounded-lg border-[1.5px] border-stroke bg-transparent px-5 py-3 font-normal text-black outline-none transition focus:border-primary active:border-primary disabled:cursor-default disabled:bg-whiter dark:border-form-strokedark dark:bg-form-input dark:text-white dark:focus:border-primary',
            }
        )
    )

