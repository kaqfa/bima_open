from django.urls import reverse
from django.shortcuts import redirect
from django.db.models import Q
from django.contrib.auth.decorators import permission_required

from BIMA.utils import form_error_redirect, form_success_redirect, is_admin_ormawa
from ormawa.models import Kegiatan, StatusKegiatan, Kepengurusan
from proposalLPJ.forms import PemasukanForm
from proposalLPJ.models import Pemasukan



class PemasukanView:
    @permission_required('ormawa.add_kegiatan')
    def create(request, kegiatanID):
        if request.method == "POST":
            
            if is_admin_ormawa(request.user):
                kepengurusan = Kepengurusan.objects.filter(Q(ketua__nim=request.user) | Q(sekretaris__nim=request.user)).first()
                kegiatan = Kegiatan.objects.filter(kepengurusan=kepengurusan, status=StatusKegiatan.USULAN_VALID , id=kegiatanID).first()
                form = PemasukanForm(request.POST)
                if form.is_valid():
                    pemasukan = form.save(commit=False)
                    pemasukan.kegiatan = kegiatan
                    pemasukan.save()
                    # return redirect('panitia.index', kegiatanID)
                    return form_success_redirect(request, message="Success Created Pemasukan", route=reverse('proposal.show', kwargs={"kegiatanID":kegiatanID}) + f'?{request.META["QUERY_STRING"]}')
                else:
                    return form_error_redirect(request, form=form, route=reverse('proposal.show', kwargs={"kegiatanID":kegiatanID}) + f'?{request.META["QUERY_STRING"]}')
                
    
    @permission_required('ormawa.change_kegiatan')
    def edit(request, kegiatanID, pemasukanID):
        if request.method == "POST":
            
            if is_admin_ormawa(request.user):
                kepengurusan = Kepengurusan.objects.filter(Q(ketua__nim=request.user) | Q(sekretaris__nim=request.user)).first()
                kegiatan = Kegiatan.objects.filter(kepengurusan=kepengurusan, status=StatusKegiatan.USULAN_VALID , id=kegiatanID).first()
                pemasukan = Pemasukan.objects.filter(kegiatan=kegiatan, id=pemasukanID).first()

                form = PemasukanForm(request.POST, instance=pemasukan)
                if form.is_valid():
                    form.save()
                    return form_success_redirect(request, message="Success Modified Pemasukan", route=reverse('proposal.show', kwargs={"kegiatanID":kegiatanID}) + f'?{request.META["QUERY_STRING"]}')
                else:
                    return form_error_redirect(request, form=form, route=reverse('proposal.show', kwargs={"kegiatanID":kegiatanID}) + f'?{request.META["QUERY_STRING"]}')
                

    @permission_required('ormawa.delete_kegiatan')
    def delete(request, kegiatanID, pemasukanID):
        if request.method ==  "POST":
            
            if is_admin_ormawa(request.user):
                kepengurusan = Kepengurusan.objects.filter(Q(ketua__nim=request.user) | Q(sekretaris__nim=request.user)).first()
                kegiatan = Kegiatan.objects.filter(kepengurusan=kepengurusan, status=StatusKegiatan.USULAN_VALID , id=kegiatanID).first()
                pemasukan = Pemasukan.objects.filter(kegiatan=kegiatan, id=pemasukanID).first()
                pemasukan.delete()
                # return redirect('panitia.index', kegiatanID)
                return form_success_redirect(request, message="Success Deleted Pemasukan", route=reverse('proposal.show', kwargs={"kegiatanID":kegiatanID}) + f'?{request.META["QUERY_STRING"]}')