from django.urls import reverse
from django.shortcuts import redirect
from django.db.models import Q
from django.contrib.auth.decorators import permission_required

from BIMA.utils import form_error_redirect, form_success_redirect, is_admin_ormawa
from mahasiswa.models import Mahasiswa
from ormawa.models import Kegiatan, StatusKegiatan, Kepengurusan
from proposalLPJ.forms import PanitiaForm
from proposalLPJ.models import Panitia


class PanitiaView:

    @permission_required('ormawa.add_kegiatan')
    def create(request, kegiatanID):
        if request.method == "POST":
            
            if is_admin_ormawa(request.user):
                kepengurusan = Kepengurusan.objects.filter(Q(ketua__nim=request.user) | Q(sekretaris__nim=request.user)).first()
                kegiatan = Kegiatan.objects.filter(kepengurusan=kepengurusan, status=StatusKegiatan.USULAN_VALID , id=kegiatanID).first()

                form = PanitiaForm(request.POST)
                if form.is_valid():
                    panitia = form.save(commit=False)
                    panitia.mahasiswa = Mahasiswa.objects.get(nim=form.cleaned_data['mahasiswa'])
                    panitia.koordinator = True if form.cleaned_data['koordinator'] == 'koordinator' else False
                    panitia.save()
                    return form_success_redirect(request, message="Success Created Panitia", route=reverse('proposal.show', kwargs={"kegiatanID":kegiatanID}) + f'?{request.META["QUERY_STRING"]}')
                else:
                    return form_error_redirect(request, form=form, route=reverse('proposal.show', kwargs={"kegiatanID":kegiatanID}) + f'?{request.META["QUERY_STRING"]}')
                
    
    @permission_required('ormawa.change_kegiatan')
    def edit(request, kegiatanID, panitiaID):
        if request.method == "POST":
            
            if is_admin_ormawa(request.user):
                kepengurusan = Kepengurusan.objects.filter(Q(ketua__nim=request.user) | Q(sekretaris__nim=request.user)).first()
                kegiatan = Kegiatan.objects.filter(kepengurusan=kepengurusan, status=StatusKegiatan.USULAN_VALID , id=kegiatanID).first()
                panitia = Panitia.objects.filter(seksi__kegiatan=kegiatan, id=panitiaID).first()

                form = PanitiaForm(request.POST, instance=panitia)
                if form.is_valid():
                    form.save()
                    return form_success_redirect(request, message="Success Modified Panitia", route=reverse('proposal.show', kwargs={"kegiatanID":kegiatanID}) + f'?{request.META["QUERY_STRING"]}')
                else:
                    return form_error_redirect(request, form=form, route=reverse('proposal.show', kwargs={"kegiatanID":kegiatanID}) + f'?{request.META["QUERY_STRING"]}')

    @permission_required('ormawa.delete_kegiatan')
    def delete(request, kegiatanID, panitiaID):
        if request.method ==  "POST":
            
            if is_admin_ormawa(request.user):
                kepengurusan = Kepengurusan.objects.filter(Q(ketua__nim=request.user) | Q(sekretaris__nim=request.user)).first()
                kegiatan = Kegiatan.objects.filter(kepengurusan=kepengurusan, status=StatusKegiatan.USULAN_VALID , id=kegiatanID).first()
                panitia = Panitia.objects.filter(seksi__kegiatan=kegiatan, id=panitiaID).first()
                panitia.delete()
                return form_success_redirect(request, message="Success Deleted Panitia", route=reverse('proposal.show', kwargs={"kegiatanID":kegiatanID}) + f'?{request.META["QUERY_STRING"]}')