import json
from django.shortcuts import HttpResponse
from django.db.models import Q
from django.views.decorators.csrf import csrf_exempt

from BIMA.utils import is_admin_ormawa
from ormawa.models import Kegiatan, StatusKegiatan, Kepengurusan


class PengantarView:
    
    @csrf_exempt
    def edit(request, kegiatanID):
        
        if is_admin_ormawa(request.user):
            kepengurusan = Kepengurusan.objects.filter(Q(ketua__nim=request.user) | Q(sekretaris__nim=request.user)).first()
            kegiatan = Kegiatan.objects.filter(kepengurusan=kepengurusan, status=StatusKegiatan.USULAN_VALID , id=kegiatanID)
            if not kegiatan:
                return HttpResponse(status=401)
                
            if request.method == "POST":
                json_data=json.loads(request.body)
                field, content = json_data['field'], json_data['content']
                if field == 'maksud_tujuan':
                    kegiatan.update(maksud_tujuan=content)
                elif field == 'latar_belakang':
                    kegiatan.update(latar_belakang=content)
                elif field == 'dasar':
                    kegiatan.update(dasar=content)
                elif field == 'penutup':
                    kegiatan.update(penutup=content)
                else:
                    return HttpResponse(status=304)
                
                return HttpResponse(status=200)