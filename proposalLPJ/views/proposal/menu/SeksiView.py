from django.urls import reverse
from django.shortcuts import redirect
from django.db.models import Q
from django.contrib.auth.decorators import permission_required

from BIMA.utils import form_error_redirect, form_success_redirect, is_admin_ormawa
from ormawa.models import Kegiatan, StatusKegiatan, Kepengurusan
from proposalLPJ.forms import SeksiForm
from proposalLPJ.models import Seksi


class SeksiView:

    @permission_required('ormawa.add_kegiatan')
    def create(request, kegiatanID):
        if request.method == "POST":
            
            if is_admin_ormawa(request.user):
                kepengurusan = Kepengurusan.objects.filter(Q(ketua__nim=request.user) | Q(sekretaris__nim=request.user)).first()
                kegiatan = Kegiatan.objects.filter(kepengurusan=kepengurusan, status=StatusKegiatan.USULAN_VALID ).first()

                form = SeksiForm(request.POST)
                if form.is_valid():
                    seksi = form.save(commit=False)
                    seksi.kegiatan = kegiatan
                    seksi.save()
                    return form_success_redirect(request, message="Success Created Seksi", route=reverse('proposal.show', kwargs={"kegiatanID":kegiatanID}) + f'?{request.META["QUERY_STRING"]}')
                else:
                    return form_error_redirect(request, form=form, route=reverse('proposal.show', kwargs={"kegiatanID":kegiatanID}) + f'?{request.META["QUERY_STRING"]}')

    
    @permission_required('ormawa.change_kegiatan')
    def edit(request, kegiatanID, seksiID):
        if request.method == "POST":

            if is_admin_ormawa(request.user):
                kepengurusan = Kepengurusan.objects.filter(Q(ketua__nim=request.user) | Q(sekretaris__nim=request.user)).first()
                kegiatan = Kegiatan.objects.filter(kepengurusan=kepengurusan, status=StatusKegiatan.USULAN_VALID , id=kegiatanID).first()
                seksi = Seksi.objects.filter(kegiatan=kegiatan, id=seksiID).first()

                form = SeksiForm(request.POST, instance=seksi)
                if form.is_valid():
                    form.save()
                    return form_success_redirect(request, message="Success Modified Seksi", route=reverse('proposal.show', kwargs={"kegiatanID":kegiatanID}) + f'?{request.META["QUERY_STRING"]}')
                else:
                    return form_error_redirect(request, form=form, route=reverse('proposal.show', kwargs={"kegiatanID":kegiatanID}) + f'?{request.META["QUERY_STRING"]}')
            

    @permission_required('ormawa.delete_kegiatan')
    def delete(request, kegiatanID, seksiID):
        if request.method ==  "POST":

            if is_admin_ormawa(request.user):
                kepengurusan = Kepengurusan.objects.filter(Q(ketua__nim=request.user) | Q(sekretaris__nim=request.user)).first()
                kegiatan = Kegiatan.objects.filter(kepengurusan=kepengurusan, status=StatusKegiatan.USULAN_VALID , id=kegiatanID).first()
                seksi = Seksi.objects.filter(kegiatan=kegiatan, id=seksiID).first()
                seksi.delete()
                return form_success_redirect(request, message="Success Deleted Seksi", route=reverse('proposal.show', kwargs={"kegiatanID":kegiatanID}) + f'?{request.META["QUERY_STRING"]}')
