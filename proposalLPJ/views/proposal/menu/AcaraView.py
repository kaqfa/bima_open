from django.urls import reverse
from django.db.models import Q
from django.contrib.auth.decorators import permission_required
from django.shortcuts import redirect

from BIMA.utils import form_error_redirect, form_success_redirect, is_admin_ormawa
from ormawa.models import Kegiatan, StatusKegiatan, Kepengurusan
from proposalLPJ.forms import AcaraForm
from proposalLPJ.models import Acara


class AcaraView:

    @permission_required('ormawa.add_kegiatan')
    def create(request, kegiatanID):
        if request.method == "POST":

            if is_admin_ormawa(request.user):
                form = AcaraForm(request.POST)
                if form.is_valid():
                    acara = form.save(commit=False)
                    kepengurusan = Kepengurusan.objects.filter(Q(ketua__nim=request.user) | Q(sekretaris__nim=request.user)).first()
                    kegiatan = Kegiatan.objects.filter(kepengurusan=kepengurusan, status=StatusKegiatan.USULAN_VALID , id=kegiatanID).first()
                    acara.kegiatan = kegiatan
                    acara.save()
                    return form_success_redirect(request, message="Success Created Acara", route=reverse('proposal.show', kwargs={"kegiatanID":kegiatanID}) + f'?{request.META["QUERY_STRING"]}')
                else:
                    return form_error_redirect(request, form=form, route=reverse('proposal.show', kwargs={"kegiatanID":kegiatanID}) + f'?{request.META["QUERY_STRING"]}')

    
    @permission_required('ormawa.change_kegiatan')
    def edit(request, kegiatanID, acaraID):
        if request.method == "POST":

            if is_admin_ormawa(request.user):
                kepengurusan = Kepengurusan.objects.get(Q(ketua__nim=request.user) | Q(sekretaris__nim=request.user))
                kegiatan = Kegiatan.objects.get(kepengurusan=kepengurusan, status=StatusKegiatan.USULAN_VALID , id=kegiatanID)
                acara = Acara.objects.get(id=acaraID, kegiatan=kegiatan)
                form = AcaraForm(request.POST, instance=acara)
                if form.is_valid():
                    form.save()
                    return form_success_redirect(request, message="Success Modified Acara", route=reverse('proposal.show', kwargs={"kegiatanID":kegiatanID}) + f'?{request.META["QUERY_STRING"]}')
                else:
                    return form_error_redirect(request, form=form, route=reverse('proposal.show', kwargs={"kegiatanID":kegiatanID}) + f'?{request.META["QUERY_STRING"]}')
            

    @permission_required('ormawa.delete_kegiatan')
    def delete(request, kegiatanID, acaraID):
        if request.method ==  "POST":

            if is_admin_ormawa(request.user):
                kepengurusan = Kepengurusan.objects.filter(Q(ketua__nim=request.user) | Q(sekretaris__nim=request.user)).first()
                kegiatan = Kegiatan.objects.filter(kepengurusan=kepengurusan, status=StatusKegiatan.USULAN_VALID , id=kegiatanID).first()
                acara = Acara.objects.get(id=acaraID, kegiatan=kegiatan)
                acara.delete()
                return form_success_redirect(request, message="Success Deleted Acara", route=reverse('proposal.show', kwargs={"kegiatanID":kegiatanID}) + f'?{request.META["QUERY_STRING"]}')
