from django.urls import reverse
from django.shortcuts import redirect
from django.db.models import Q
from django.contrib.auth.decorators import permission_required

from BIMA.utils import form_error_redirect, form_success_redirect, is_admin_ormawa
from ormawa.models import Kegiatan, StatusKegiatan, Kepengurusan
from proposalLPJ.forms import AnggaranForm
from proposalLPJ.models import Anggaran



class AnggaranView:

    @permission_required('ormawa.add_kegiatan')
    def create(request, kegiatanID):
        if request.method == "POST":
            
            if is_admin_ormawa(request.user):
                kepengurusan = Kepengurusan.objects.filter(Q(ketua__nim=request.user) | Q(sekretaris__nim=request.user)).first()
                kegiatan = Kegiatan.objects.filter(kepengurusan=kepengurusan, status=StatusKegiatan.USULAN_VALID , id=kegiatanID).first()
                form = AnggaranForm(request.POST)
                if form.is_valid():
                    anggaran = form.save(commit=False)
                    anggaran.kegiatan = kegiatan
                    anggaran.save()
                    return form_success_redirect(request, message="Success Created Anggaran", route=reverse('proposal.show', kwargs={"kegiatanID":kegiatanID}) + f'?{request.META["QUERY_STRING"]}')
                else:
                    return form_error_redirect(request, form=form, route=reverse('proposal.show', kwargs={"kegiatanID":kegiatanID}) + f'?{request.META["QUERY_STRING"]}')
    
    @permission_required('ormawa.change_kegiatan')
    def edit(request, kegiatanID, anggaranID):
        if request.method == "POST":
            
            if is_admin_ormawa(request.user):
                kepengurusan = Kepengurusan.objects.filter(Q(ketua__nim=request.user) | Q(sekretaris__nim=request.user)).first()
                kegiatan = Kegiatan.objects.filter(kepengurusan=kepengurusan, status=StatusKegiatan.USULAN_VALID , id=kegiatanID).first()
                anggaran = Anggaran.objects.filter(kegiatan=kegiatan, id=anggaranID).first()

                form = AnggaranForm(request.POST, instance=anggaran)
                if form.is_valid():
                    form.save()
                    return form_success_redirect(request, message="Success Modified Anggaran", route=reverse('proposal.show', kwargs={"kegiatanID":kegiatanID}) + f'?{request.META["QUERY_STRING"]}')
                else:
                    return form_error_redirect(request, form=form, route=reverse('proposal.show', kwargs={"kegiatanID":kegiatanID}) + f'?{request.META["QUERY_STRING"]}')
                

    @permission_required('ormawa.delete_kegiatan')
    def delete(request, kegiatanID, anggaranID):
        if request.method ==  "POST":
            
            if is_admin_ormawa(request.user):
                kepengurusan = Kepengurusan.objects.filter(Q(ketua__nim=request.user) | Q(sekretaris__nim=request.user)).first()
                kegiatan = Kegiatan.objects.filter(kepengurusan=kepengurusan, status=StatusKegiatan.USULAN_VALID , id=kegiatanID).first()
                anggaran = Anggaran.objects.filter(kegiatan=kegiatan, id=anggaranID).first()
                anggaran.delete()
                return form_success_redirect(request, message="Success Deleted Anggaran", route=reverse('proposal.show', kwargs={"kegiatanID":kegiatanID}) + f'?{request.META["QUERY_STRING"]}')