from .proposal.ProposalView import ProposalView
from .proposal.menu.AcaraView import AcaraView
from .proposal.menu.AnggaranView import AnggaranView
from .proposal.menu.PanitiaView import PanitiaView
from .proposal.menu.PemasukanView import PemasukanView
from .proposal.menu.PengantarView import PengantarView
from .proposal.menu.SeksiView import SeksiView

from .lpj.LPJView import LPJView
from .lpj.menu.BuktiPengeluaranView import BuktiPengeluaranView
from .lpj.menu.DokumentasiView import DokumentasiView
from .lpj.menu.HasilView import HasilView
from .lpj.menu.RealisasiAcaraView import RealisasiAcaraView
from .lpj.menu.RealisasiDanaAkademikView import RealisasiDanaAkademikView
from .lpj.menu.RealisasiPengeluaranView import RealisasiPengeluaranView