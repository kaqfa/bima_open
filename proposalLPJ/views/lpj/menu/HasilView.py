from django.urls import reverse
from django.shortcuts import redirect, HttpResponse
from django.db.models import Q
from django.contrib.auth.decorators import permission_required

from BIMA.utils import is_admin_ormawa
from ormawa.models import Kegiatan, StatusKegiatan, Kepengurusan
from proposalLPJ.forms import HasilForm


class HasilView:
    
    @permission_required('ormawa.change_kegiatan')
    def edit(request, kegiatanID):
        if request.method == "POST":

            if is_admin_ormawa(request.user):
                kepengurusan = Kepengurusan.objects.filter(Q(ketua__nim=request.user) | Q(sekretaris__nim=request.user)).first()
                kegiatan = Kegiatan.objects.filter(kepengurusan=kepengurusan, status=StatusKegiatan.PROPOSAL_VALID, id=kegiatanID).first()
                if not kegiatan:
                    return HttpResponse(status=401)

                form = HasilForm(request.POST, instance=kegiatan)
                if form.is_valid():
                    form.save()
                    return redirect(reverse('lpj.show', kwargs={"kegiatanID":kegiatanID}) + f'?{request.META["QUERY_STRING"]}')
                