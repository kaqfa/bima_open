from django.urls import reverse
import os
from django.conf import settings
from django.shortcuts import redirect, render, HttpResponse
from django.db.models import Q
from django.contrib.auth.decorators import permission_required

from BIMA.utils import form_error_redirect, form_success_redirect, is_admin_ormawa
from ormawa.models import Kegiatan, StatusKegiatan, Kepengurusan
from proposalLPJ.forms import RealisasiPengeluaranForm, RealisasiPengeluaranTambahanForm
from proposalLPJ.models import Anggaran



class RealisasiPengeluaranView:

    @permission_required('ormawa.change_kegiatan')
    def create(request, kegiatanID):
        if request.method == "POST":

            if is_admin_ormawa(request.user):
                kepengurusan = Kepengurusan.objects.filter(Q(ketua__nim=request.user) | Q(sekretaris__nim=request.user)).first()
                kegiatan = Kegiatan.objects.get(Q(status=StatusKegiatan.PROPOSAL_VALID) & Q(kepengurusan=kepengurusan) & Q(id=kegiatanID))

                form = RealisasiPengeluaranTambahanForm(request.POST)
                if form.is_valid():
                    anggaran = form.save(commit=False)
                    anggaran.qty = anggaran.real_qty
                    anggaran.harga = anggaran.real_harga
                    anggaran.kegiatan = kegiatan
                    anggaran.tambahan = True
                    anggaran.save()
                    return form_success_redirect(request, message="Success Created Realisasi Pengelauran", route=reverse('lpj.show', kwargs={"kegiatanID":kegiatanID}) + f'?{request.META["QUERY_STRING"]}')
                else:
                    return form_error_redirect(request, form=form, route=reverse('lpj.show', kwargs={"kegiatanID":kegiatanID}) + f'?{request.META["QUERY_STRING"]}')
            
    
    @permission_required('ormawa.change_kegiatan')
    def edit(request, kegiatanID, anggaranID):
        if request.method == "POST":

            if is_admin_ormawa(request.user):
                kepengurusan = Kepengurusan.objects.filter(Q(ketua__nim=request.user) | Q(sekretaris__nim=request.user)).first()
                kegiatan = Kegiatan.objects.get(Q(status=StatusKegiatan.PROPOSAL_VALID) & Q(kepengurusan=kepengurusan) & Q(id=kegiatanID))
                anggaran = Anggaran.objects.get(kegiatan=kegiatan, id=anggaranID)

                form = RealisasiPengeluaranForm(request.POST, instance=anggaran)
                if form.is_valid():
                    form.save()
                    return form_success_redirect(request, message="Success Modified Realisasi Pengelauran", route=reverse('lpj.show', kwargs={"kegiatanID":kegiatanID}) + f'?{request.META["QUERY_STRING"]}')
                else:
                    return form_error_redirect(request, form=form, route=reverse('lpj.show', kwargs={"kegiatanID":kegiatanID}) + f'?{request.META["QUERY_STRING"]}')
                