from django.urls import reverse
from django.shortcuts import redirect
from django.db.models import Q
from django.contrib.auth.decorators import permission_required

from BIMA.utils import form_error_redirect, form_success_redirect, is_admin_ormawa
from ormawa.models import Kegiatan, StatusKegiatan, Kepengurusan
from proposalLPJ.models import Acara
from proposalLPJ.forms import RealisasiAcaraForm, RealisasiAcaraTambahanForm



class RealisasiAcaraView:

    @permission_required('ormawa.change_kegiatan')
    def create(request, kegiatanID):
        if request.method == "POST":

            if is_admin_ormawa(request.user):
                form = RealisasiAcaraTambahanForm(request.POST)
                if form.is_valid():
                    acara = form.save(commit=False)
                    kepengurusan = Kepengurusan.objects.filter(Q(ketua__nim=request.user) | Q(sekretaris__nim=request.user)).first()
                    kegiatan = Kegiatan.objects.filter(Q(status=StatusKegiatan.PROPOSAL_VALID) & Q(kepengurusan=kepengurusan) & Q(id=kegiatanID)).first()
                    acara.waktu_mulai = acara.real_waktu_mulai
                    acara.waktu_selesai = acara.real_waktu_selesai
                    acara.kegiatan = kegiatan
                    acara.tambahan = True
                    acara.save()

                    return form_success_redirect(request, message="Success Created Realisasi Acara", route=reverse('lpj.show', kwargs={"kegiatanID":kegiatanID}) + f'?{request.META["QUERY_STRING"]}')
                else:
                    return form_error_redirect(request, form=form, route=reverse('lpj.show', kwargs={"kegiatanID":kegiatanID}) + f'?{request.META["QUERY_STRING"]}')

    
    @permission_required('ormawa.change_kegiatan')
    def edit(request, kegiatanID, acaraID):
        if request.method == "POST":

            if is_admin_ormawa(request.user):
                kepengurusan = Kepengurusan.objects.get(Q(ketua__nim=request.user) | Q(sekretaris__nim=request.user))
                kegiatan = Kegiatan.objects.get(Q(status=StatusKegiatan.PROPOSAL_VALID) & Q(kepengurusan=kepengurusan) & Q(id=kegiatanID))
                acara = Acara.objects.get(id=acaraID, kegiatan=kegiatan)
                form = RealisasiAcaraForm(request.POST, instance=acara)
                if form.is_valid():
                    form.save()
                    return form_success_redirect(request, message="Success Modified Realisasi Acara", route=reverse('lpj.show', kwargs={"kegiatanID":kegiatanID}) + f'?{request.META["QUERY_STRING"]}')
                else:
                    return form_error_redirect(request, form=form, route=reverse('lpj.show', kwargs={"kegiatanID":kegiatanID}) + f'?{request.META["QUERY_STRING"]}')