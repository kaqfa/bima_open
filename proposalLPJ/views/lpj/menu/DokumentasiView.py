from django.urls import reverse
import os
from django.conf import settings
from django.shortcuts import redirect
from django.db.models import Q
from django.contrib.auth.decorators import permission_required

from BIMA.utils import form_error_redirect, form_success_redirect, is_admin_ormawa
from ormawa.models import Kegiatan, StatusKegiatan, Kepengurusan
from proposalLPJ.forms import DokumentasiForm
from proposalLPJ.models import Dokumentasi


    

class DokumentasiView:

    @permission_required('ormawa.add_kegiatan')
    def create(request, kegiatanID):
        if request.method == "POST":
            
            if is_admin_ormawa(request.user):
                form = DokumentasiForm(request.POST, request.FILES)
                kepengurusan = Kepengurusan.objects.filter(Q(ketua__nim=request.user) | Q(sekretaris__nim=request.user)).first()
                kegiatan = Kegiatan.objects.filter(kepengurusan=kepengurusan, status=StatusKegiatan.PROPOSAL_VALID, id=kegiatanID).first()
                
                if form.is_valid():
                    dokumentasi = form.save(commit=False)
                    dokumentasi.kegiatan = kegiatan
                    dokumentasi.save()
                    # return redirect(reverse('lpj.show', kwargs={"kegiatanID":kegiatanID}) + f'?{request.META["QUERY_STRING"]}')
                    return form_success_redirect(request, message="Success Created Dokumentasi", route=reverse('lpj.show', kwargs={"kegiatanID":kegiatanID}) + f'?{request.META["QUERY_STRING"]}')
                else:
                    return form_error_redirect(request, form=form, route=reverse('lpj.show', kwargs={"kegiatanID":kegiatanID}) + f'?{request.META["QUERY_STRING"]}')
        
    @permission_required('ormawa.change_kegiatan')
    def edit(request, kegiatanID, dokumentasiID):
        if request.method == "POST":
    
            if is_admin_ormawa(request.user):
                kepengurusan = Kepengurusan.objects.get(Q(ketua__nim=request.user) | Q(sekretaris__nim=request.user))
                kegiatan = Kegiatan.objects.get(kepengurusan=kepengurusan, status=StatusKegiatan.PROPOSAL_VALID, id=kegiatanID)
                dokumentasi = Dokumentasi.objects.get(id=dokumentasiID, kegiatan=kegiatan)
                oldPhoto = dokumentasi.file_foto.path

                form = DokumentasiForm(request.POST, request.FILES, instance=dokumentasi)
                if form.is_valid():
                    form.save(commit=False)
                    if os.path.isfile(os.path.join(settings.MEDIA_ROOT, oldPhoto)):  
                        os.remove(os.path.join(settings.MEDIA_ROOT, oldPhoto))
                    form.save()
                    # return redirect(reverse('lpj.show', kwargs={"kegiatanID":kegiatanID}) + f'?{request.META["QUERY_STRING"]}')
                    return form_success_redirect(request, message="Success Modified Dokumentasi", route=reverse('lpj.show', kwargs={"kegiatanID":kegiatanID}) + f'?{request.META["QUERY_STRING"]}')
                else:
                    return form_error_redirect(request, form=form, route=reverse('lpj.show', kwargs={"kegiatanID":kegiatanID}) + f'?{request.META["QUERY_STRING"]}')
                

    @permission_required('ormawa.delete_kegiatan')
    def delete(request, kegiatanID, dokumentasiID):
        if request.method ==  "POST":
            
            if is_admin_ormawa(request.user):
                kepengurusan = Kepengurusan.objects.filter(Q(ketua__nim=request.user) | Q(sekretaris__nim=request.user)).first()
                kegiatan = Kegiatan.objects.filter(kepengurusan=kepengurusan, status=StatusKegiatan.PROPOSAL_VALID, id=kegiatanID).first()
                dokumentasi = Dokumentasi.objects.get(id=dokumentasiID, kegiatan=kegiatan)
                oldPhoto = dokumentasi.file_foto.path

                if os.path.isfile(os.path.join(settings.MEDIA_ROOT, oldPhoto)):  
                        os.remove(os.path.join(settings.MEDIA_ROOT, oldPhoto))
                dokumentasi.delete()
                # return redirect(reverse('lpj.show', kwargs={"kegiatanID":kegiatanID}) + f'?{request.META["QUERY_STRING"]}')
                return form_success_redirect(request, message="Success Deleted Dokumentasi", route=reverse('lpj.show', kwargs={"kegiatanID":kegiatanID}) + f'?{request.META["QUERY_STRING"]}')
            