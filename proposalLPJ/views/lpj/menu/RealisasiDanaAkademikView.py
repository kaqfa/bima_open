from django.urls import reverse
from django.shortcuts import redirect
from django.db.models import Q
from django.contrib.auth.decorators import permission_required

from BIMA.utils import form_error_redirect, form_success_redirect, is_admin_ormawa
from ormawa.models import Kegiatan, StatusKegiatan, Kepengurusan
from proposalLPJ.forms import RealisasiDanaAkademikForm, RealisasiDanaAkademikTambahanForm
from proposalLPJ.models import Pemasukan



class RealisasiDanaAkademikView:

    @permission_required('ormawa.add_kegiatan')
    def create(request, kegiatanID):
        if request.method == "POST":

            if is_admin_ormawa(request.user):
                kepengurusan = Kepengurusan.objects.filter(Q(ketua__nim=request.user) | Q(sekretaris__nim=request.user)).first()
                kegiatan = Kegiatan.objects.filter(Q(status=StatusKegiatan.PROPOSAL_VALID) & Q(kepengurusan=kepengurusan) & Q(id=kegiatanID)).first()
                form = RealisasiDanaAkademikTambahanForm(request.POST)
                if form.is_valid():
                    pemasukan = form.save(commit=False)
                    pemasukan.kegiatan = kegiatan
                    pemasukan.total_dana = pemasukan.real_total_dana
                    pemasukan.tambahan = True
                    pemasukan.save()
                    return form_success_redirect(request, message="Success Created Realisasi Dana Akademik", route=reverse('lpj.show', kwargs={"kegiatanID":kegiatanID}) + f'?{request.META["QUERY_STRING"]}')
                else:
                    return form_error_redirect(request, form=form, route=reverse('lpj.show', kwargs={"kegiatanID":kegiatanID}) + f'?{request.META["QUERY_STRING"]}')
            

    @permission_required('ormawa.change_kegiatan')
    def edit(request, kegiatanID, pemasukanID):
        if request.method == "POST":

            if is_admin_ormawa(request.user):
                kepengurusan = Kepengurusan.objects.get(Q(ketua__nim=request.user) | Q(sekretaris__nim=request.user))
                kegiatan = Kegiatan.objects.get(kepengurusan=kepengurusan, status=StatusKegiatan.PROPOSAL_VALID , id=kegiatanID)
                pemasukan = Pemasukan.objects.get(id=pemasukanID, kegiatan=kegiatan)
                form = RealisasiDanaAkademikForm(request.POST, instance=pemasukan)
                if form.is_valid():
                    form.save()
                    return form_success_redirect(request, message="Success Modified Realisasi Dana Akademik", route=reverse('lpj.show', kwargs={"kegiatanID":kegiatanID}) + f'?{request.META["QUERY_STRING"]}')
                else:
                    return form_error_redirect(request, form=form, route=reverse('lpj.show', kwargs={"kegiatanID":kegiatanID}) + f'?{request.META["QUERY_STRING"]}')
        