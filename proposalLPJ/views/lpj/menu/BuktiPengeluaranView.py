from django.urls import reverse
import os
from django.conf import settings
from django.shortcuts import redirect
from django.db.models import Q
from django.contrib.auth.decorators import permission_required

from BIMA.utils import form_error_redirect, form_success_redirect, is_admin_ormawa
from ormawa.models import Kegiatan, StatusKegiatan, Kepengurusan
from proposalLPJ.forms import BuktiPengeluaranForm
from proposalLPJ.models import BuktiPengeluaran



class BuktiPengeluaranView:
    
    @permission_required('ormawa.add_kegiatan')
    def create(request, kegiatanID):
        if request.method == "POST":

            if is_admin_ormawa(request.user):
                form = BuktiPengeluaranForm(request.POST, request.FILES)
                kepengurusan = Kepengurusan.objects.filter(Q(ketua__nim=request.user) | Q(sekretaris__nim=request.user)).first()
                kegiatan = Kegiatan.objects.get(Q(status=StatusKegiatan.PROPOSAL_VALID) & Q(kepengurusan=kepengurusan))
                if form.is_valid():
                    buktiPengeluaran = form.save(commit=False)
                    buktiPengeluaran.kegiatan = kegiatan
                    buktiPengeluaran.save()
                    # return redirect(reverse('lpj.show', kwargs={"kegiatanID":kegiatanID}) + f'?{request.META["QUERY_STRING"]}')
                    return form_success_redirect(request, message="Success Created Bukti Pengeluaran", route=reverse('lpj.show', kwargs={"kegiatanID":kegiatanID}) + f'?{request.META["QUERY_STRING"]}')
                else:
                    return form_error_redirect(request, form=form, route=reverse('lpj.show', kwargs={"kegiatanID":kegiatanID}) + f'?{request.META["QUERY_STRING"]}')
        
    @permission_required('ormawa.change_kegiatan')
    def edit(request, kegiatanID, pemasukanID):
        if request.method == "POST":
            if is_admin_ormawa(request.user):
                kepengurusan = Kepengurusan.objects.get(Q(ketua__nim=request.user) | Q(sekretaris__nim=request.user))
                kegiatan = Kegiatan.objects.get(kepengurusan=kepengurusan, status=StatusKegiatan.PROPOSAL_VALID, id=kegiatanID)
                buktiPengeluaran = BuktiPengeluaran.objects.get(id=pemasukanID, kegiatan=kegiatan)
                oldPhoto = buktiPengeluaran.file_foto.path

                form = BuktiPengeluaranForm(request.POST, request.FILES, instance=buktiPengeluaran)
                if form.is_valid():
                    form.save(commit=False)
                    print(oldPhoto)
                    if os.path.isfile(os.path.join(settings.MEDIA_ROOT, oldPhoto)):  
                        os.remove(os.path.join(settings.MEDIA_ROOT, oldPhoto))
                    form.save()
                    # return redirect(reverse('lpj.show', kwargs={"kegiatanID":kegiatanID}) + f'?{request.META["QUERY_STRING"]}')
                    return form_success_redirect(request, message="Success Modified Bukti Pengeluaran", route=reverse('lpj.show', kwargs={"kegiatanID":kegiatanID}) + f'?{request.META["QUERY_STRING"]}')
                else:
                    return form_error_redirect(request, form=form, route=reverse('lpj.show', kwargs={"kegiatanID":kegiatanID}) + f'?{request.META["QUERY_STRING"]}')
                

    @permission_required('ormawa.change_kegiatan')
    def delete(request, kegiatanID, pemasukanID):
        if request.method ==  "POST":
            
            if is_admin_ormawa(request.user):
                kepengurusan = Kepengurusan.objects.filter(Q(ketua__nim=request.user) | Q(sekretaris__nim=request.user)).first()
                kegiatan = Kegiatan.objects.filter(kepengurusan=kepengurusan, status=StatusKegiatan.PROPOSAL_VALID, id=kegiatanID).first()
                buktiPengeluaran = BuktiPengeluaran.objects.get(id=pemasukanID, kegiatan=kegiatan)
                oldPhoto = buktiPengeluaran.file_foto.path

                if os.path.isfile(os.path.join(settings.MEDIA_ROOT, oldPhoto)):  
                        os.remove(os.path.join(settings.MEDIA_ROOT, oldPhoto))
                buktiPengeluaran.delete()
                # return redirect(reverse('lpj.show', kwargs={"kegiatanID":kegiatanID}) + f'?{request.META["QUERY_STRING"]}')
                return form_success_redirect(request, message="Success Deleted Bukti Pengeluaran", route=reverse('lpj.show', kwargs={"kegiatanID":kegiatanID}) + f'?{request.META["QUERY_STRING"]}')
            