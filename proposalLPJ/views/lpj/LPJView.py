from django.shortcuts import redirect, render, HttpResponse
from django.db.models import Q
from django.contrib.auth.decorators import permission_required
from django.template.loader import get_template
from django.urls import reverse
from django.utils import timezone

import locale
from weasyprint import HTML

from BIMA import settings
from BIMA.decorators import group_required
from BIMA.utils import is_admin_kemahasiswaan, is_admin_ormawa
from ormawa.models import Kegiatan, Kepengurusan, StatusKegiatan
from proposalLPJ.forms import BuktiPengeluaranForm, DokumentasiForm, HasilForm, RealisasiAcaraForm, RealisasiDanaAkademikForm, RealisasiDanaAkademikTambahanForm, RealisasiPengeluaranForm, RealisasiAcaraTambahanForm, RealisasiPengeluaranTambahanForm
from proposalLPJ.models import Acara, Anggaran, BuktiPengeluaran, Dokumentasi, Pemasukan, Seksi



class LPJView:
    @permission_required('ormawa.view_kegiatan')
    def index(request):
        if is_admin_ormawa(request.user):
            kepengurusan = Kepengurusan.objects.filter(Q(ketua__nim=request.user) | Q(sekretaris__nim=request.user)).first()
            kegiatan = Kegiatan.objects.filter(Q(status=StatusKegiatan.PROPOSAL_VALID) | Q(status=StatusKegiatan.LPJ) | Q(status=StatusKegiatan.LPJ_VALID) & Q(kepengurusan=kepengurusan))
        elif is_admin_kemahasiswaan(request.user):
            kegiatan = Kegiatan.objects.filter(Q(status=StatusKegiatan.LPJ) | Q(status=StatusKegiatan.LPJ_VALID) )
        else:
            kegiatan = None

        context = {
            "kegiatan": kegiatan,
        }
        return render(request, 'lpj/index.html', context)


    @permission_required('ormawa.view_kegiatan')
    def show(request, kegiatanID):

        if is_admin_ormawa(request.user):
            kepengurusan = Kepengurusan.objects.filter(Q(ketua__nim=request.user) | Q(sekretaris__nim=request.user)).first()
            kegiatan = Kegiatan.objects.filter(Q(status=StatusKegiatan.PROPOSAL_VALID) | Q(status=StatusKegiatan.LPJ) & Q(kepengurusan=kepengurusan) & Q(id=kegiatanID)).first()
            if not kegiatan:
                return HttpResponse("LPJ Not Found")
            
            context = {
                "LPJName": kegiatan.nama,
                "LPJID": kegiatanID,
                "kegiatan": kegiatan,
                "acara": Acara.objects.filter(kegiatan=kegiatan),
                "anggaran": Anggaran.objects.filter(kegiatan=kegiatan),
                "pemasukan": Pemasukan.objects.filter(kegiatan=kegiatan),
                "buktiPengeluaran": BuktiPengeluaran.objects.filter(kegiatan=kegiatan),
                "dokumentasi": Dokumentasi.objects.filter(kegiatan=kegiatan),

                "HasilForm": HasilForm,
                "RealisasiAcaraForm": RealisasiAcaraForm,
                "RealisasiAcaraTambahanForm": RealisasiAcaraTambahanForm,

                "RealisasiPengeluaranForm": RealisasiPengeluaranForm,
                "RealisasiPengeluaranTambahanForm": RealisasiPengeluaranTambahanForm,

                "RealisasiDanaAkademikForm": RealisasiDanaAkademikForm,
                "RealisasiDanaAkademikTambahanForm": RealisasiDanaAkademikTambahanForm,

                "BuktiPengeluaranForm": BuktiPengeluaranForm,
                "DokumentasiForm": DokumentasiForm,

            }

            return render(request, 'lpj/show.html', context)
    

    @permission_required('ormawa.change_kegiatan')
    def submit(request, kegiatanID):
        if request.method == "GET":
            if is_admin_ormawa(request.user):
                kepengurusan = Kepengurusan.objects.filter(Q(ketua__nim=request.user) | Q(sekretaris__nim=request.user)).first()
                kegiatan = Kegiatan.objects.filter(Q(status=StatusKegiatan.PROPOSAL_VALID) | Q(status=StatusKegiatan.LPJ) & Q(kepengurusan=kepengurusan) & Q(id=kegiatanID)).first()
                kegiatan.status = "LPJ"
                kegiatan.save()

                return redirect(reverse('lpj.show', kwargs={"kegiatanID":kegiatanID}) + f'?{request.META["QUERY_STRING"]}')

    
    @group_required('BIMA UNIVERSITAS')
    @permission_required('ormawa.change_kegiatan')
    def admin_verify(request, kegiatanID):
        if request.method == "POST":
            if is_admin_ormawa(request.user):
                kegiatan = Kegiatan.objects.filter(id=kegiatanID).first()
                kegiatan.status = "LPJ Valid" if kegiatan.status == "LPJ" else "LPJ"
                kegiatan.save() 

                return redirect('lpj.index')
        
    
    @permission_required('ormawa.view_kegiatan')
    def print(request, kegiatanID):
        if is_admin_ormawa(request.user) or is_admin_kemahasiswaan(request.user):
            kepengurusan = Kepengurusan.objects.filter(Q(ketua__nim=request.user) | Q(sekretaris__nim=request.user)).first()
            kegiatan = Kegiatan.objects.filter(Q(status=StatusKegiatan.PROPOSAL) | Q(status=StatusKegiatan.PROPOSAL_VALID) | Q(status=StatusKegiatan.LPJ) | Q(status=StatusKegiatan.LPJ_VALID) | Q(kepengurusan=kepengurusan) & Q(id=kegiatanID)).first()
            if not kegiatan:
                return HttpResponse("Proposal Not Found")
            
            totalAnggaran = 0
            seksi = Seksi.objects.filter(kegiatan=kegiatan)
            for s in seksi:
                for a in s.anggaran.all():
                    totalAnggaran += a.total
                    print(a.total)
                
            pemasukan = Pemasukan.objects.filter(kegiatan=kegiatan)
            totalPemasukan = 0
            for p in pemasukan:
                totalPemasukan += p.total_dana

            saldo = totalPemasukan - totalAnggaran

            acara =Acara.objects.filter(kegiatan=kegiatan).order_by('waktu_mulai')
            acara_perhari = {}
            last_tanggal = ""
            locale.setlocale(locale.LC_TIME, 'id_ID.UTF-8')
            for a in acara:
                waktu_mulai_local = timezone.localtime(a.real_waktu_mulai)
                waktu_selesai_local = timezone.localtime(a.real_waktu_selesai)

                tanggal = waktu_mulai_local.strftime("%A, %e %B %Y")
                if tanggal != last_tanggal:
                    last_tanggal = tanggal
                    acara_perhari[tanggal] = []

                acara_perhari[last_tanggal].append( {
                    'waktuMulai': waktu_mulai_local.strftime("%H:%M"),
                    'waktuSelesai': waktu_selesai_local.strftime("%H:%M"),
                    'nama': a.nama,
                    'seksi': a.seksi.nama,
                })

            dokumentasi = Dokumentasi.objects.filter(kegiatan=kegiatan)
            buktiPengeluaran = BuktiPengeluaran.objects.filter(kegiatan=kegiatan)               

            template = get_template('lpj/print/print_lpj.html')
            context = {
                'kegiatan': kegiatan,

                'acara_perhari': acara_perhari,

                'dokumentasi': dokumentasi,

                'buktiPengeluaran': buktiPengeluaran,

                'seksi': seksi,
                'totalAnggaran': totalAnggaran,

                'pemasukan': pemasukan,
                'totalPemasukan': totalPemasukan,

                'saldo': saldo,

                'verified': True if kegiatan.status == StatusKegiatan.PROPOSAL_VALID else False,
                'full_url': request.build_absolute_uri(settings.MEDIA_URL),
            }

            html_content = template.render(context)
            pdf_file = HTML(string=html_content).write_pdf()

            response = HttpResponse(pdf_file, content_type='application/pdf')
            response['Content-Disposition'] = f'filename="{kegiatan.nama}_lpj.pdf"'
            return response
            # return render(request, 'lpj/print/print_lpj.html', context)
            