from django import forms
from .models import *


class OrmawaForm(forms.ModelForm):
    class Meta:
        model = Ormawa
        fields = [
            'nama', 
            'singkatan',
            'deskripsi',
            'level', 
            'status', 
        ]
        widgets = {
            'status': forms.Select(
                attrs={
                    'x-model': 'ormawaEdit.status',
                    'class': 'w-full rounded-md border-[1.5px] border-stroke bg-transparent px-5 py-2 font-normal text-black outline-none transition focus:border-primary active:border-primary disabled:cursor-default disabled:bg-whiter dark:border-form-strokedark dark:bg-form-input dark:text-white dark:focus:border-primary',
                }
            ),
            'level': forms.Select(
                attrs={
                    'x-model': 'ormawaEdit.level',
                    'class': 'w-full rounded-md border-[1.5px] border-stroke bg-transparent px-5 py-2 font-normal text-black outline-none transition focus:border-primary active:border-primary disabled:cursor-default disabled:bg-whiter dark:border-form-strokedark dark:bg-form-input dark:text-white dark:focus:border-primary',
                }
            ),
        }
    nama = forms.CharField(
        label="Nama",
        widget=forms.TextInput(
            attrs={
                'x-model': 'ormawaEdit.nama',
                'class': 'w-full rounded-md border-[1.5px] border-stroke bg-transparent px-5 py-2 font-normal text-black outline-none transition focus:border-primary active:border-primary disabled:cursor-default disabled:bg-whiter dark:border-form-strokedark dark:bg-form-input dark:text-white dark:focus:border-primary',
            }
        )
    )
    singkatan = forms.CharField(
        label="Singkatan",
        widget=forms.TextInput(
            attrs={
                'x-model': 'ormawaEdit.singkatan',
                'class': 'w-full rounded-md border-[1.5px] border-stroke bg-transparent px-5 py-2 font-normal text-black outline-none transition focus:border-primary active:border-primary disabled:cursor-default disabled:bg-whiter dark:border-form-strokedark dark:bg-form-input dark:text-white dark:focus:border-primary',
            }
        )
    )
    deskripsi = forms.CharField(
        label="Deskripsi",
        widget=forms.Textarea(
            attrs={
                'x-model': 'ormawaEdit.deskripsi',
                'rows': '3', 
                'class': 'w-full rounded-md border-[1.5px] border-stroke bg-transparent px-5 py-2 font-normal text-black outline-none transition focus:border-primary active:border-primary disabled:cursor-default disabled:bg-whiter dark:border-form-strokedark dark:bg-form-input dark:text-white dark:focus:border-primary',
            }
        )
    )
    
class LevelForm(forms.ModelForm):
    class Meta:
        model = Level
        fields = ['nama']
        widgets = {
            'nama': forms.TextInput(attrs={
                'x-model': "levelOrmawaEdit.nama",
                'class': 'w-full rounded-md border-[1.5px] border-stroke bg-transparent px-5 py-2 font-normal text-black outline-none transition focus:border-primary active:border-primary disabled:cursor-default disabled:bg-whiter dark:border-form-strokedark dark:bg-form-input dark:text-white dark:focus:border-primary',
                }),
        }
 
class PeriodeForm(forms.ModelForm):
    class Meta:     
        model = Periode
        fields = [
            'nama', 
            'tgl_mulai', 
            'tgl_selesai',
            'start_rkt_univ',
            'end_rkt_univ',
            'start_rkt_fakultas',
            'end_rkt_fakultas',
        ]

    nama = forms.CharField(
        label="Nama",
        widget=forms.TextInput(
            attrs={
                'x-model': 'periodeEdit.nama',
                'class': 'w-full rounded-md border-[1.5px] border-stroke bg-transparent px-5 py-2 font-normal text-black outline-none transition focus:border-primary active:border-primary disabled:cursor-default disabled:bg-whiter dark:border-form-strokedark dark:bg-form-input dark:text-white dark:focus:border-primary',
            }
        )
    )
    tgl_mulai = forms.DateField(
        label="Tanggal Mulai",
        widget=forms.DateInput(
            attrs={
                'x-model': 'periodeEdit.tgl_mulai',
                'class': 'w-full rounded-md border-[1.5px] border-stroke bg-transparent px-5 py-2 font-normal text-black outline-none transition focus:border-primary active:border-primary disabled:cursor-default disabled:bg-whiter dark:border-form-strokedark dark:bg-form-input dark:text-white dark:focus:border-primary',
                'type': 'date'
            }
        )
    )
    tgl_selesai = forms.DateField(
        label="Tanggal Selesai",
        widget=forms.DateInput(
            attrs={
                'x-model': 'periodeEdit.tgl_selesai',
                'class': 'w-full rounded-md border-[1.5px] border-stroke bg-transparent px-5 py-2 font-normal text-black outline-none transition focus:border-primary active:border-primary disabled:cursor-default disabled:bg-whiter dark:border-form-strokedark dark:bg-form-input dark:text-white dark:focus:border-primary',
                'type': 'date',
            }
        )
    )
    start_rkt_univ = forms.DateTimeField(
        label="Start RKT Univ",
        widget=forms.DateTimeInput(
            attrs={
                'x-model': 'periodeEdit.start_rkt_univ',
                'class': 'w-full rounded-md border-[1.5px] border-stroke bg-transparent px-5 py-2 font-normal text-black outline-none transition focus:border-primary active:border-primary disabled:cursor-default disabled:bg-whiter dark:border-form-strokedark dark:bg-form-input dark:text-white dark:focus:border-primary',
                'type': 'datetime-local',
            }
        )
    )
    end_rkt_univ = forms.DateTimeField(
        label="End RKT Univ",
        widget=forms.DateTimeInput(
            attrs={
                'x-model': 'periodeEdit.end_rkt_univ',
                'class': 'w-full rounded-md border-[1.5px] border-stroke bg-transparent px-5 py-2 font-normal text-black outline-none transition focus:border-primary active:border-primary disabled:cursor-default disabled:bg-whiter dark:border-form-strokedark dark:bg-form-input dark:text-white dark:focus:border-primary',
                'type': 'datetime-local',
            }
        )
    )
    start_rkt_fakultas = forms.DateTimeField(
        label="Start RKT Fakultas",
        widget=forms.DateTimeInput(
            attrs={
                'x-model': 'periodeEdit.start_rkt_fakultas',
                'class': 'w-full rounded-md border-[1.5px] border-stroke bg-transparent px-5 py-2 font-normal text-black outline-none transition focus:border-primary active:border-primary disabled:cursor-default disabled:bg-whiter dark:border-form-strokedark dark:bg-form-input dark:text-white dark:focus:border-primary',
                'type': 'datetime-local',
            }
        )
    )
    end_rkt_fakultas = forms.DateTimeField(
        label="End RKT Fakultas",
        widget=forms.DateTimeInput(
            attrs={
                'x-model': 'periodeEdit.end_rkt_fakultas',
                'class': 'w-full rounded-md border-[1.5px] border-stroke bg-transparent px-5 py-2 font-normal text-black outline-none transition focus:border-primary active:border-primary disabled:cursor-default disabled:bg-whiter dark:border-form-strokedark dark:bg-form-input dark:text-white dark:focus:border-primary',
                'type': 'datetime-local',
            }
        )
    )
    