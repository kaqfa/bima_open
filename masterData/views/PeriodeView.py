from django.shortcuts import render, redirect
from django.contrib.auth.decorators import permission_required
from django.contrib import messages

from BIMA.utils import form_error_redirect, form_success_redirect, is_admin_kemahasiswaan
from masterData.forms import PeriodeForm
from masterData.models import Periode


class PeriodeView:

    @permission_required('ormawa.view_periode')
    def index(request):
        if is_admin_kemahasiswaan(request.user):    
            form = PeriodeForm()
            periode = Periode.objects.all()

            context = {
                'form': form,
                'periode': periode,
            }
            return render(request, 'periode/index.html', context)

    @permission_required('ormawa.add_periode')
    def create(request):
        if request.method == 'POST':
            if is_admin_kemahasiswaan(request.user):    
                form = PeriodeForm(request.POST)
                if form.is_valid():
                    form.save()
                    
                    return form_success_redirect(request, message="Success Created Periode", route='periode.index')
                else:
                    return form_error_redirect(request, form=form, route='periode.index')

    @permission_required('ormawa.change_periode')
    def edit(request, periodeID):
        if request.method == 'POST':
            if is_admin_kemahasiswaan(request.user):
                periode = Periode.objects.filter(id=periodeID).first()
                form = PeriodeForm(request.POST, instance=periode)
                if form.is_valid():
                    form.save()

                    return form_success_redirect(request, message="Success Modified Periode", route='periode.index')
                else:
                    return form_error_redirect(request, form=form, route='periode.index')
        
    @permission_required('ormawa.delete_periode')
    def delete(request, periodeID):
        if is_admin_kemahasiswaan(request.user):
            if request.method == 'POST':
                Periode.objects.filter(id=periodeID).delete()    

                return form_success_redirect(request, message="Success Deleted Periode", route='periode.index')

