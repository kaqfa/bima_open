import json
from django.http import JsonResponse
from django.shortcuts import render, redirect
from django.contrib.auth.decorators import permission_required
from django.contrib import messages

from BIMA.utils import form_error_redirect, form_success_redirect
from masterData.forms import OrmawaForm, PeriodeForm
from masterData.models import Level, Ormawa
from ormawa.forms import KepengurusanForm
from ormawa.models import Kepengurusan


class OrmawaView:
    @permission_required('ormawa.view_ormawa')
    def index(request):
        ormawaForm = OrmawaForm()
        ormawa = Ormawa.objects.all()

        level = Level.objects.all()

        context = {
            "ormawa": ormawa,
            "level": level,
            "form": ormawaForm,
        }
        return render(request, 'ormawa/index.html', context)
    
    @permission_required('ormawa.view_ormawa')
    def show(request, ormawaID):
        ormawa = Ormawa.objects.filter(id=ormawaID).first()
        kepengurusan = Kepengurusan.objects.filter(ormawa_id=ormawaID).order_by('-periode__tgl_selesai')

        periodeForm = PeriodeForm()
        kepengurusanForm = KepengurusanForm()

        context = {
            "ormawa": ormawa,
            "kepengurusan": kepengurusan,
            "periodeForm": periodeForm,
            "kepengurusanForm": kepengurusanForm,
        }

        return render(request, 'user/ormawa/show.html', context)
    
    @permission_required('ormawa.view_ormawa')
    def search(request):
        if request.method == "POST":
            body = json.loads(request.body)
            ormawa = Ormawa.objects.filter(nama__icontains=body['query'])

            data = []
            for o in ormawa:
                data.append({
                    'singkatan': o.singkatan,
                    'nama': o.nama,
                })
            return JsonResponse(data, safe=False)

    @permission_required('ormawa.add_ormawa')
    def create(request):
        if request.method == "POST":
            form = OrmawaForm(request.POST)
            if form.is_valid():
                ormawa = form.save(commit=False)
                ormawa.save()
                
                return form_success_redirect(request, message="Success Created Ormawa", route='ormawa.index')
            else:
                return form_error_redirect(request, form=form, route='ormawa.index')
    

    @permission_required('ormawa.change_ormawa')
    def edit(request, ormawaID):
        ormawa = Ormawa.objects.filter(id=ormawaID).first()

        if request.method == "POST":
            form = OrmawaForm(request.POST, instance=ormawa)
            if form.is_valid():
                ormawa = form.save(commit=False)
                ormawa.save()

                return form_success_redirect(request, message="Success Modified Ormawa", route='ormawa.index')
            else:
                return form_error_redirect(request, form=form, route='ormawa.index')
            

    @permission_required('ormawa.delete_ormawa')
    def delete(request, ormawaID):
        Ormawa.objects.filter(id=ormawaID).delete()

        return form_success_redirect(request, message="Success Deleted Ormawa", route='ormawa.index')
