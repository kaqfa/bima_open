from django.http import JsonResponse
from django.db.models import Q
from rest_framework.response import Response

from rest_framework.views import APIView
from rest_framework import status

from masterData.models import Ormawa
from masterData.serializers import RequestGetOrmawa

class OrmawaSearchAPI(APIView):
    
    def post(self, request, *args, **kwargs):
        serializer = RequestGetOrmawa(data=request.data)
        if not serializer.is_valid():
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
        
        data = serializer.validated_data.get('data')
        ormawas = Ormawa.objects.filter(Q(singkatan__icontains=data.get('query')) | 
                                      Q(nama__icontains=data.get('query')))[:5]
        
        print(data.get('query'))
        print(ormawas)
        if not ormawas:
            return Response([], status=status.HTTP_200_OK)
        
        result = []
        for o in ormawas:
            result.append(
                {
                    'id': o.id,
                    'code': o.singkatan,
                    'nama': o.nama,
                }
            )
        return JsonResponse(result, safe=False)