from django.shortcuts import render, redirect
from django.contrib.auth.decorators import permission_required
from django.contrib import messages

from BIMA.utils import form_error_redirect, form_success_redirect
from masterData.forms import LevelForm
from masterData.models import Level

class LevelView:
    @permission_required('ormawa.view_level')
    def index(request):
        level = Level.objects.all()
        levelForm = LevelForm()

        context = {
            "level": level,
            "form": levelForm,
        }
        return render(request, "level/index.html",context)

    @permission_required('ormawa.add_level')
    def create(request):
        if request.method == "POST":
            form = LevelForm(request.POST)
            if form.is_valid():
                level = form.save(commit=False)
                level.save()

                return form_success_redirect(request, message="Success Created Jenis Ormawa", route='level.index')
            else:
                return form_error_redirect(request, form=form, route='level.index')
        
    
    @permission_required('ormawa.change_level')
    def edit(request, levelID):
        if request.method == "POST":
            level = Level.objects.filter(id=levelID).first()

            form = LevelForm(request.POST, instance=level)
            if form.is_valid():
                level = form.save(commit=False)
                level.save()

                return form_success_redirect(request, message="Success Modified Jenis Ormawa", route='level.index')
            else:
                return form_error_redirect(request, form=form, route='level.index')


    @permission_required('ormawa.delete_level')
    def delete(request, levelID):
        Level.objects.filter(id=levelID).delete()

        return form_success_redirect(request, message="Success Created Jenis Ormawa", route='level.index')