from django.db import models

# Create your models here.
class Level(models.Model):
    id = models.AutoField(primary_key=True)
    nama = models.CharField(max_length=100)

    class Meta:
        verbose_name_plural = "Level Ormawa"

    def __str__(self):
        return f'{self.nama}'

class StatusOrmawa(models.TextChoices):
    AKTIF = 'Aktif'
    NON_AKTIF = 'Non Aktif'

class Ormawa(models.Model):
    id = models.AutoField(primary_key=True)
    nama = models.CharField(max_length=100)
    singkatan = models.CharField(max_length=20)
    deskripsi = models.TextField(null=True, blank=True)
    sejarah = models.TextField(null=True, blank=True)
    visi = models.TextField(null=True, blank=True)
    misi = models.TextField(null=True, blank=True)
    alamat = models.CharField(max_length=200, null=True, blank=True)
    foto = models.ImageField(null=True, blank=True)
    website = models.CharField(max_length=200, null=True, blank=True)
    media_sosial = models.CharField(max_length=200, null=True, blank=True)
    tele = models.CharField(max_length=200, null=True, blank=True)
    level = models.ForeignKey(Level, on_delete=models.CASCADE)
    status = models.CharField(max_length=10, choices=StatusOrmawa)

    class Meta:
        verbose_name_plural = "Ormawa"

    def __str__(self):
        return f'{self.singkatan}'

class Periode(models.Model):
    id = models.AutoField(primary_key=True)
    nama = models.CharField(max_length=100)
    tgl_mulai = models.DateField()
    tgl_selesai = models.DateField()
    start_rkt_univ = models.DateTimeField(null=True, blank=True)
    end_rkt_univ = models.DateTimeField(null=True, blank=True)
    start_rkt_fakultas = models.DateTimeField(null=True, blank=True)
    end_rkt_fakultas = models.DateTimeField(null=True, blank=True)
    
    class Meta:
        verbose_name_plural = "Periode"

    def __str__(self):
        return f'{self.nama}'
