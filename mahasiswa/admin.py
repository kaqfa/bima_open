from django.contrib import admin

from .models import Mahasiswa, Prestasi

@admin.register(Mahasiswa)
class MahasiswaAdmin(admin.ModelAdmin):
    list_display = ['nim', 'nama']

@admin.register(Prestasi)
class PrestasiAdmin(admin.ModelAdmin):
    list_display = ['nama', 'tingkat', 'mahasiswa']
