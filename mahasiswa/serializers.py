from rest_framework import serializers
from .models import Mahasiswa

class MahasiswaSerializer(serializers.ModelSerializer):
    class Meta:
        model = Mahasiswa
        fields = ['nama', 'nim', 'tgl_lahir_ortu']

class RequestSerializer(serializers.Serializer):
    auth = serializers.DictField(child=serializers.CharField())
    data = serializers.ListField(child=MahasiswaSerializer())
    
    
class RequestGetMahasiswa(serializers.Serializer):
    data = serializers.DictField(child=serializers.CharField())