from django.urls import path
from django.contrib.auth.decorators import login_required
from . import views


urlpatterns = [
    path("dashboard/mahasiswa", login_required(views.MahasiswaView.index), name="mahasiswa.index"),
    path("dashboard/mahasiswa/search", login_required(views.MahasiswaView.search), name="mahasiswa.search"),

    path("dashboard/prestasi", login_required(views.PrestasiView.index), name="prestasi.index"),
    path("dashboard/prestasi/create", login_required(views.PrestasiView.create), name="prestasi.create"),
    path("dashboard/prestasi/edit/<int:prestasiID>", login_required(views.PrestasiView.edit), name="prestasi.edit"),
    path("dashboard/prestasi/delete/<int:prestasiID>", login_required(views.PrestasiView.delete), name="prestasi.delete"),
    
    path("api/mahasiswa", views.MahasiswaAPI.as_view(), name="api.mahasiswa.insert_bulk"), 
    path("api/mahasiswa/search", views.MahasiswaSearchAPI.as_view(), name="api.mahasiswa.search")    
]