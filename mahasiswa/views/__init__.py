from .MahasiswaView import MahasiswaView
from .PrestasiView import PrestasiView

from .api.MahasiswaAPI import MahasiswaAPI
from .api.MahasiswaSearchAPI import MahasiswaSearchAPI