from django.shortcuts import render
from django.http import JsonResponse
from django.contrib.auth.decorators import permission_required
import json

from mahasiswa.models import Mahasiswa

class MahasiswaView:

    @permission_required('mahasiswa.view_mahasiswa')
    def index(request):
        mahasiswa = Mahasiswa.objects.all()

        context = {
            "mahasiswa": mahasiswa,
        }
        return render(request, 'mahasiswa/index.html', context)

    def search(request):
        if request.method == "POST":
            if request.content_type == 'text/plain':
                body = json.loads(request.body)
                mahasiswa = Mahasiswa.objects.filter(nim__icontains=body['query']).all()
                
                data = []
                for m in mahasiswa:
                    data.append({
                        'id': m.id,
                        'nama': f"{m.nim} - {m.nama}",
                    })
                return JsonResponse(data, safe=False)
            else:
                print(request.content_type)
                query_search = request.POST.get('query')

                mahasiswa = Mahasiswa.objects.filter(nim=query_search).first()
                print(mahasiswa)

                context = {
                    "found": True if mahasiswa != 0 else False,
                    "mahasiswa": mahasiswa,
                }
                return render(request, 'mahasiswa/search.html', context)

        else:
            return render(request, 'mahasiswa/search.html')
