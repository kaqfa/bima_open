from django.shortcuts import render, redirect
from django.contrib.auth.decorators import permission_required

from mahasiswa.forms import PrestasiForm
from mahasiswa.models import Mahasiswa, Prestasi



class PrestasiView:
    
    @permission_required('mahasiswa.view_prestasi')
    def index(request):
        context = {
            "prestasi": Prestasi.objects.all(),
            "form": PrestasiForm(),
        }
        return render(request, 'prestasi/index.html', context)
    
    @permission_required('mahasiswa.add_prestasi')
    def create(request):
        if request.method == "POST":
            form = PrestasiForm(request.POST)
            if form.is_valid():
                Prestasi.objects.create(
                    nama=form.cleaned_data['nama'],
                    tingkat=form.cleaned_data['tingkat'],
                    mahasiswa=Mahasiswa.objects.get(nim=form.cleaned_data['mahasiswa'])
                )
                return redirect('prestasi.index')
    
    @permission_required('mahasiswa.change_prestasi')
    def edit(request, prestasiID):
        if request.method == "POST":
            form = PrestasiForm(request.POST)
            if form.is_valid():
                print(form.cleaned_data['nama'])
                prestasi = Prestasi.objects.filter(id=prestasiID).first()
                prestasi.nama = form.cleaned_data['nama']
                prestasi.tingkat = form.cleaned_data['tingkat']
                prestasi.mahasiswa = Mahasiswa.objects.get(nim=form.cleaned_data['mahasiswa'])
                prestasi.save()

                return redirect('prestasi.index')

    @permission_required('mahasiswa.delete_prestasi')
    def delete(request, prestasiID):
        if request.method == "POST":
            Prestasi.objects.filter(id=prestasiID).delete()
            return redirect('prestasi.index')
