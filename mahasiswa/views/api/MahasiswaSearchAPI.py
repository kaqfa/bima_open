from django.http import JsonResponse
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status

from django.db.models import Q

from mahasiswa.models import Mahasiswa
from mahasiswa.serializers import RequestGetMahasiswa

class MahasiswaSearchAPI(APIView):
    def post(self, request, *args, **kwargs):
        serializer = RequestGetMahasiswa(data=request.data)
        if not serializer.is_valid():
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
        
        data = serializer.validated_data.get('data')
        mahasiswas = Mahasiswa.objects\
                    .filter(Q(nim__icontains=data.get('query')) | 
                            Q(nama__icontains=data.get('query')))[:5]
        if not mahasiswas:
            return Response([], status=status.HTTP_200_OK)
        
        result = []
        for m in mahasiswas:
            result.append(
                {
                    'id': m.id,
                    'code': m.nim,
                    'nama': m.nama,
                }
            )
            
        return JsonResponse(result, safe=False)