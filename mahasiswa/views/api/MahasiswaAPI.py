
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
from django.contrib import auth

from mahasiswa.serializers import RequestSerializer
from mahasiswa.models import Mahasiswa
from mahasiswa.serializers import MahasiswaSerializer


class MahasiswaAPI(APIView):
    
    def post(self, request, *args, **kwargs):
        serializer = RequestSerializer(data=request.data)
        if not serializer.is_valid():
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
        
        # Extract
        auth_data = serializer.validated_data.get('auth')
        data = serializer.validated_data.get('data')
        
        # Auth
        user = auth.authenticate(request, username=auth_data.get("username"), password=auth_data.get("password"))
        if user is None:
            return Response("Autentikasi gagal", status=status.HTTP_401_UNAUTHORIZED)
        
        # Gather
        bulk_mahasiswa = []
        for mahasiswa_data in data:
            mahasiswa_serializer = MahasiswaSerializer(data=mahasiswa_data)
            if mahasiswa_serializer.is_valid():
                bulk_mahasiswa.append(mahasiswa_serializer.validated_data)
            else:
                return Response(mahasiswa_serializer.errors, status=status.HTTP_400_BAD_REQUEST)
        
        # Insert
        Mahasiswa.objects.bulk_create([
            Mahasiswa(**mahasiswa) for mahasiswa in bulk_mahasiswa
        ], ignore_conflicts=True)
        
        return Response(bulk_mahasiswa, status=status.HTTP_200_OK)