from django.http import JsonResponse
from django.db.models import Q
from requests import Response

from rest_framework.views import APIView
from rest_framework import status

from dosen.models import Dosen
from dosen.serializers import RequestGetDosen

class DosenSearchAPI(APIView):
    
    def post(self, request, *args, **kwargs):
        serializer = RequestGetDosen(data=request.data)
        if not serializer.is_valid():
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
        
        data = serializer.validated_data.get('data')
        dosens = Dosen.objects.filter(Q(npp__icontains=data.get('query')) | 
                                      Q(nama__icontains=data.get('query')))[:5]
        if not dosens:
            return Response([], status=status.HTTP_200_OK)
        
        result = []
        for d in dosens:
            result.append(
                {
                    'id': d.id,
                    'code': d.npp,
                    'nama': d.nama,
                }
            )
        return JsonResponse(result, safe=False)