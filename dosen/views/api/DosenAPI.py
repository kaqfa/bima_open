
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
from django.contrib import auth

from dosen.models import Dosen
from dosen.serializers import DosenSerializer, RequestSerializer


# Create your views here.

class DosenAPI(APIView):
    def post(self, request, *args, **kwargs):
        serializer = RequestSerializer(data=request.data)
        if not serializer.is_valid():
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
        
        # Extract
        auth_data = serializer.validated_data.get('auth')
        data = serializer.validated_data.get('data')
        
        # Auth
        user = auth.authenticate(request, username=auth_data.get("username"), password=auth_data.get("password"))
        if user is None:
            return Response("Autentikasi gagal", status=status.HTTP_401_UNAUTHORIZED)
        
        # Gather
        bulk_mahasiswa = []
        for dosen_data in data:
            dosen_serializer = DosenSerializer(data=dosen_data)
            if dosen_serializer.is_valid():
                bulk_mahasiswa.append(dosen_serializer.validated_data)
            else:
                return Response(dosen_serializer.errors, status=status.HTTP_400_BAD_REQUEST)
        
        # Insert
        Dosen.objects.bulk_create([
            Dosen(**dosen) for dosen in bulk_mahasiswa
        ], ignore_conflicts=True)
        
        return Response("Data diterima", status=status.HTTP_200_OK)
    