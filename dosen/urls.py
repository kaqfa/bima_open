from django.urls import path
from . import views


urlpatterns = [
    path("api/dosen", views.DosenAPI.as_view(), name="api.dosen.insert_bulk"),
    path("api/dosen/search", views.DosenSearchAPI.as_view(), name="api.dosen.search")
]