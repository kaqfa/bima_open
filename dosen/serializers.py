from rest_framework import serializers
from .models import Dosen

class DosenSerializer(serializers.ModelSerializer):
    class Meta:
        model = Dosen
        fields = ['nama', 'npp']

class RequestSerializer(serializers.Serializer):
    auth = serializers.DictField(child=serializers.CharField())
    data = serializers.ListField(child=DosenSerializer())
    
class RequestGetDosen(serializers.Serializer):
    data = serializers.DictField(child=serializers.CharField())