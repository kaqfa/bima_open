from django.db import models
from django.contrib.auth.models import User

# Create your models here.
class Dosen(models.Model):
    id = models.AutoField(primary_key=True)
    npp = models.CharField(max_length=20, unique=True)
    nama = models.CharField(max_length=100)
    user = models.ForeignKey(User, related_name='dosen', on_delete=models.CASCADE, null=True, blank=True)

    def __str__(self):
        return f'{self.npp} - {self.nama}'