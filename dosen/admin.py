from django.contrib import admin
from .models import Dosen

# Register your models here.
@admin.register(Dosen)
class DosenAdmin(admin.ModelAdmin):
    list_display = ['npp', 'nama']