from django.shortcuts import render, redirect
from django.db.models import Q
from django.contrib.auth.decorators import permission_required

from BIMA.utils import is_admin_ormawa, is_admin_kemahasiswaan
from django.contrib import messages

from ormawa.forms import *
from ormawa.models import Kepengurusan, Pengurus


class PengurusView:
    def index(request):
        form = PengurusForm()
        kepengurusan = Kepengurusan.objects.filter(Q(ketua__nim=request.user) | Q(sekretaris__nim=request.user)).first()
        pengurus = Pengurus.objects.filter(kepengurusan=kepengurusan)

        context = {
            "form": form,
            "pengurus": pengurus,
            "kepengurusan": kepengurusan,
        }
        return render(request, 'pengurus/index.html', context)

    @permission_required('ormawa.add_pengurus')
    def create(request, kepengurusanID):
        if request.method == "POST":
            form = PengurusForm(request.POST)
            if is_admin_kemahasiswaan(request.user):
                if form.is_valid():
                    pengurus = form.save(commit=False)
                    pengurus.mahasiswa = Mahasiswa.objects.get(nim=form.cleaned_data['mahasiswa'])
                    pengurus.kepengurusan_id = kepengurusanID
                    pengurus.save()

                    return redirect('kepengurusan.show', pengurus.kepengurusan.ormawa.singkatan, pengurus.kepengurusan.periode.id)
                else:
                    for field, errors in form.errors.items():
                        for error in errors:
                            messages.error(request, f"[Error] {field}: {error}")
                        return redirect('kepengurusan.show', pengurus.kepengurusan.ormawa.singkatan, pengurus.kepengurusan.periode.id)


            elif is_admin_ormawa(request.user):
                if form.is_valid():
                    pengurus = form.save(commit=False)
                    pengurus.mahasiswa = Mahasiswa.objects.get(nim=form.cleaned_data['mahasiswa'])
                    kepengurusan = Kepengurusan.objects.filter(Q(ketua__nim=request.user) | Q(sekretaris__nim=request.user)).first()
                    pengurus.kepengurusan = kepengurusan
                    pengurus.save()

                    return redirect('kepengurusan.show', pengurus.kepengurusan.ormawa.singkatan, pengurus.kepengurusan.periode.id)
                else:
                    for field, errors in form.errors.items():
                        for error in errors:
                            messages.error(request, f"[Error] {field}: {error}")
                        return redirect('kepengurusan.show', pengurus.kepengurusan.ormawa.singkatan, pengurus.kepengurusan.periode.id)

    @permission_required('ormawa.change_pengurus')
    def edit(request, pengurusID):
        if request.method == "POST":

            if is_admin_kemahasiswaan(request.user):
                pengurus = Pengurus.objects.filter(id=pengurusID).first()
            elif is_admin_ormawa(request.user):
                kepengurusan = Kepengurusan.objects.filter(Q(ketua__nim=request.user) | Q(sekretaris__nim=request.user)).first()
                pengurus = Pengurus.objects.filter(id=pengurusID, kepengurusan=kepengurusan).first()
            
            form = PengurusForm(request.POST, instance=pengurus)
            if form.is_valid():
                pengurus.mahasiswa = Mahasiswa.objects.get(nim=form.cleaned_data['mahasiswa'])
                pengurus = form.save(commit=False)
                pengurus.save() 

                return redirect('kepengurusan.show', pengurus.kepengurusan.ormawa.singkatan, pengurus.kepengurusan.periode.id)
            
            else:
                for field, errors in form.errors.items():
                    for error in errors:
                        messages.error(request, f"[Error] {field}: {error}")
                    return redirect('kepengurusan.show', pengurus.kepengurusan.ormawa.singkatan, pengurus.kepengurusan.periode.id)
                

    @permission_required('ormawa.delete_pengurus')
    def delete(request, pengurusID):
        if request.method == "POST":

            if is_admin_kemahasiswaan(request.user):
                pengurus = Pengurus.objects.filter(id=pengurusID).first()
            elif is_admin_ormawa(request.user):
                kepengurusan = Kepengurusan.objects.filter(Q(ketua__nim=request.user) | Q(sekretaris__nim=request.user)).first()
                pengurus = Pengurus.objects.filter(id=pengurusID, kepengurusan=kepengurusan).first()

            pengurus.delete()

            return redirect('kepengurusan.show', pengurus.kepengurusan.ormawa.singkatan, pengurus.kepengurusan.periode.id)
