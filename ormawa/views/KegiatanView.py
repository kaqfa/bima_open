from django.contrib.auth.decorators import permission_required
from django.shortcuts import render, redirect
from django.db.models import Q

from BIMA.utils import form_error_redirect, form_success_redirect, is_admin_kemahasiswaan, is_admin_ormawa

from mahasiswa.models import Mahasiswa

from masterData.models import Ormawa
from ormawa.forms import KegiatanDKForm, KegiatanForm
from ormawa.models import Kegiatan, StatusKegiatan, Kepengurusan


class RKTView:

    @permission_required('ormawa.view_kegiatan')
    def index(request):
        
        if is_admin_kemahasiswaan(request.user):
        
            kegiatan = Kegiatan.objects.filter((Q(status=StatusKegiatan.USULAN) | Q(status=StatusKegiatan.USULAN_VALID)) & Q(jenis__nama_jenis="RKT"))
            form = KegiatanForm()
            context = {
                "form": form,
                "kegiatan": kegiatan,
            }
            return render(request, 'kegiatan/rkt.html', context)

        elif is_admin_ormawa(request.user):
            form = KegiatanForm()
            kepengurusan = Kepengurusan.objects.filter(Q(ketua__nim=request.user) | Q(sekretaris__nim=request.user)).first()
            kegiatan = Kegiatan.objects.filter(kepengurusan=kepengurusan, jenis__nama_jenis="RKT")
            
            context = {
                "form": form,
                "kegiatan": kegiatan,
            }
            return render(request, 'kegiatan/rkt.html', context)

        
        else:
            return
    


    @permission_required('ormawa.add_kegiatan')
    def create(request):
        if request.method == "POST":

            if is_admin_ormawa(request.user):
                
                form = KegiatanForm(request.POST)
                
                if form.is_valid():
                    kegiatan = form.save(commit=False)
                    kegiatan.mhs_pj = Mahasiswa.objects.get(nim=form.cleaned_data['mhs_pj'])
                    kegiatan.mhs_koord = Mahasiswa.objects.get(nim=form.cleaned_data['mhs_koord'])

                    kepengurusan = Kepengurusan.objects.filter(Q(ketua__nim=request.user) | Q(sekretaris__nim=request.user)).first()
                    kegiatan.kepengurusan = kepengurusan
                    kegiatan.ormawa = kepengurusan.ormawa
                    kegiatan.periode = kepengurusan.periode
                    kegiatan.jenis_id = 1 
                    kegiatan.status = StatusKegiatan.USULAN
                    kegiatan.save()
                    return form_success_redirect(request, message="Success Created RKT", route='kegiatan.index')
                else:
                    return form_error_redirect(request, form=form, route='kegiatan.index')
    


    @permission_required('ormawa.change_kegiatan')
    def edit(request, kegiatanID):
        if request.method == "POST":

            if is_admin_ormawa(request.user):
                
                kepengurusan = Kepengurusan.objects.filter(Q(ketua__nim=request.user) | Q(sekretaris__nim=request.user)).first()
                kegiatan = Kegiatan.objects.filter(id=kegiatanID, kepengurusan=kepengurusan).first()
                form = KegiatanForm(request.POST, instance=kegiatan)

                if form.is_valid():
                    form.save()
                    return form_success_redirect(request, message="Success Modified RKT", route='kegiatan.index')
                else:
                    return form_error_redirect(request, form=form, route='kegiatan.index')



    @permission_required('ormawa.delete_kegiatan')
    def delete(request, kegiatanID):
        if request.method ==  "POST":

            if is_admin_ormawa(request.user):
                
                kepengurusan = Kepengurusan.objects.filter(Q(ketua__nim=request.user) | Q(sekretaris__nim=request.user)).first()
                kegiatan = Kegiatan.objects.filter(id=kegiatanID, kepengurusan=kepengurusan).first()
                kegiatan.delete()

                return form_success_redirect(request, message="Success Deleted RKT", route='kegiatan.index')


    @permission_required('ormawa.edit_kegiatan')
    def admin_verify(request, kegiatanID):
        if request.method == "POST":

            if is_admin_kemahasiswaan(request.user):
                
                kegiatan = Kegiatan.objects.filter(id=kegiatanID).first()
                kegiatan.status = "Usulan Valid" if kegiatan.status == "Usulan" else "Usulan"
                kegiatan.save() 

                return redirect('kegiatan.index')


class DutaKampusView:

    @permission_required('ormawa.view_kegiatan')
    def index(request):

        if is_admin_kemahasiswaan(request.user):

            form = KegiatanDKForm()

            kegiatan = Kegiatan.objects.filter(jenis__nama_jenis="Duta Kampus")

            context = {
                "form": form,
                "kegiatan": kegiatan,
            }
            return render(request, 'kegiatan/duta_kampus.html', context)
        
        if is_admin_ormawa(request.user):

            form = KegiatanDKForm()

            kepengurusan = Kepengurusan.objects.filter(Q(ketua__nim=request.user) | Q(sekretaris__nim=request.user)).first()
            kegiatan = Kegiatan.objects.filter(kepengurusan=kepengurusan, jenis__nama_jenis="Duta Kampus")

            context = {
                "form": form,
                "kegiatan": kegiatan,
            }
            return render(request, 'kegiatan/duta_kampus.html', context)
    


    @permission_required('ormawa.add_kegiatan')
    def create(request):
        if request.method == "POST":

            if is_admin_kemahasiswaan(request.user):

                form = KegiatanDKForm(request.POST)
                
                if form.is_valid():
                    kegiatan = form.save(commit=False)
                    kegiatan.ormawa = Ormawa.objects.get(nama=form.cleaned_data['ormawa'].split(" - ")[-1])
                    kegiatan.kepengurusan = kegiatan.ormawa.kepengurusan_ormawa.order_by('-id').first()
                    kegiatan.periode = kegiatan.kepengurusan.periode
                    
                    kegiatan.mhs_pj = Mahasiswa.objects.get(nim=form.cleaned_data['mhs_pj'])
                    kegiatan.mhs_koord = Mahasiswa.objects.get(nim=form.cleaned_data['mhs_koord'])
                    
                    kegiatan.jenis_id = 2
                    kegiatan.status = StatusKegiatan.USULAN_VALID
                    kegiatan.save()
                    # return redirect('dutaKampus.index')

                    return form_success_redirect(request, message="Success Created Duta Kampus", route='dutaKampus.index')
                else:
                    return form_error_redirect(request, form=form, route='dutaKampus.index')
    


    @permission_required('ormawa.change_kegiatan')
    def edit(request, dutaKampusID):
        if request.method == "POST":
    
            if is_admin_kemahasiswaan(request.user):

                kegiatan = Kegiatan.objects.filter(id=dutaKampusID).first()
                
                form = KegiatanDKForm(request.POST, instance=kegiatan)
                if form.is_valid():
                    kegiatan = form.save(commit=False)
                    kegiatan.ormawa = Ormawa.objects.get(nama=form.cleaned_data['ormawa'].split(" - ")[-1])
                    kegiatan.kepengurusan = kegiatan.ormawa.kepengurusan_ormawa.order_by('-id').first()
                    kegiatan.periode = kegiatan.kepengurusan.periode
                    
                    kegiatan.mhs_pj = Mahasiswa.objects.get(nim=form.cleaned_data['mhs_pj'])
                    kegiatan.mhs_koord = Mahasiswa.objects.get(nim=form.cleaned_data['mhs_koord'])
                    
                    kegiatan.jenis_id = 2
                    kegiatan.status = StatusKegiatan.USULAN_VALID
                    
                    form.save()
                    
                    return form_success_redirect(request, message="Success Modified Duta Kampus", route='dutaKampus.index')
                else:
                    return form_error_redirect(request, form=form, route='dutaKampus.index')
    


    @permission_required('ormawa.delete_kegiatan')
    def delete(request, dutaKampusID):
        if request.method ==  "POST":

            if is_admin_kemahasiswaan(request.user):

                kegiatan = Kegiatan.objects.filter(id=dutaKampusID).first()
                kegiatan.delete()

                return form_success_redirect(request, message="Success Deleted Duta Kampus", route='dutaKampus.index')