from django.shortcuts import render, redirect
from django.db.models import Q
import os

from BIMA.utils import form_error_redirect, form_success_redirect, is_admin_ormawa
from ormawa.forms import *
from ormawa.models import Kepengurusan


class ProfileOrmawaView:
    
    def index(request):
        kepengurusan = Kepengurusan.objects.filter(Q(ketua__nim=request.user) | Q(sekretaris__nim=request.user)).first()
        ormawaForm = EditOrmawaForm()

        context = {
            "ormawa": kepengurusan.ormawa,
            "form": ormawaForm,
        }
        return render(request, 'profileOrmawa/index.html', context)

    def edit(request):
        if request.method == 'POST':
            if is_admin_ormawa(request.user):
                kepengurusan = Kepengurusan.objects.filter(Q(ketua__nim=request.user) | Q(sekretaris__nim=request.user)).first()
                ormawa = kepengurusan.ormawa
                oldFoto = ormawa.foto.path
                form = EditOrmawaForm(request.POST, request.FILES, instance=ormawa)
                if form.is_valid():
                    profileOrmawa = form.save(commit=False)
                    profileOrmawa.level = profileOrmawa.level
                    profileOrmawa.status = profileOrmawa.status

                    if str(profileOrmawa.foto) not in oldFoto:
                        try:
                            os.remove(oldFoto) # delete old image
                        except FileNotFoundError:
                            None # File not Found
                    profileOrmawa.save()

                    # return redirect('profileOrmawa.index')
                    return form_success_redirect(request, message="Success Modified Profile Ormawa", route='profileOrmawa.index')
                else:
                    return form_error_redirect(request, form=form, route='profileOrmawa.index')       

