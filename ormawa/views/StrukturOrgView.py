from django.shortcuts import render, redirect
from django.http import JsonResponse, HttpResponse
from django.db.models import Q

from BIMA.utils import form_error_redirect, is_admin_kemahasiswaan, is_admin_ormawa
from ormawa.forms import *
from ormawa.models import Kepengurusan, StrukturOrg


class StrukturOrgView:

    def index(request):
        form = StrukturOrgForm()

        kepengurusan = Kepengurusan.objects.filter(Q(ketua__nim=request.user) | Q(ketua__nim=request.user)).first()
        strukturOrg = StrukturOrg.objects.filter(ormawa=kepengurusan.ormawa)

        context = {
            "form": form,
            "strukturOrg": strukturOrg
        }
        return render(request, "strukturOrg/index.html", context)


    def create(request, ormawaID):
        if request.method == "POST":
            form = StrukturOrgForm(request.POST)
            if form.is_valid():
                if is_admin_kemahasiswaan(request.user):
                    strukturOrg = form.save(commit=False)
                    strukturOrg.active = True
                    strukturOrg.ormawa_id = ormawaID
                    strukturOrg.save()
                    
                    return JsonResponse({
                        'id': strukturOrg.id,
                        'nama': strukturOrg.nama,
                        'active': 'True' if strukturOrg.active else 'False',
                    })
                elif is_admin_ormawa(request.user):
                    kepengurusan = Kepengurusan.objects.filter(Q(ketua__nim=request.user) | Q(sekretaris__nim=request.user)).first()
                    strukturOrg = form.save(commit=False)
                    strukturOrg.ormawa = kepengurusan.ormawa
                    strukturOrg.active = True
                    strukturOrg.save()

                    return redirect('strukturorg.index')
            else:
                return form_error_redirect(request, form=form, route='strukturorg.index')       

            
    def edit(request, strukturOrgID):
        if request.method == "POST":
            if is_admin_kemahasiswaan(request.user):
                # edit status
                strukturOrg = StrukturOrg.objects.filter(id=strukturOrgID).first()
                strukturOrg.active = int(request.POST["status"])
                strukturOrg.save()
                
                return JsonResponse({
                        'id': strukturOrg.id,
                        'nama': strukturOrg.nama,
                        'active': 'True' if strukturOrg.active else 'False',
                    })
            
            elif is_admin_ormawa(request.user):
                # edit status and name
                kepengurusan = Kepengurusan.objects.filter(Q(ketua__nim=request.user) | Q(ketua__nim=request.user)).first()
                strukturOrg = StrukturOrg.objects.filter(id=strukturOrgID, ormawa=kepengurusan.ormawa).first()
                form = StrukturOrgForm(request.POST, instance=strukturOrg)
                if form.is_valid():
                    strukturOrg.active = 1
                    strukturOrg.save()

                    return redirect('strukturorg.index')
                else:
                    return form_error_redirect(request, form=form, route='strukturorg.index')       
            
        return
    
    def delete(request, strukturOrgID):
        if request.method == "POST":
            if is_admin_kemahasiswaan(request.user):
                strukturOrg = StrukturOrg.objects.filter(id=strukturOrgID).first()
                strukturOrg.delete()
                
                return JsonResponse({
                        'status': 'success',
                    })
            
            elif is_admin_ormawa(request.user):
                kepengurusan = Kepengurusan.objects.filter(Q(ketua__nim=request.user) | Q(ketua__nim=request.user)).first()
                strukturOrg = StrukturOrg.objects.filter(id=strukturOrgID, ormawa=kepengurusan.ormawa).first()
                strukturOrg.delete()
                
                return redirect('strukturorg.index')
