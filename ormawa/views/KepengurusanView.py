from django.shortcuts import render, redirect
from django.db.models import Q
from django.http import JsonResponse
from django.contrib.auth.decorators import permission_required

from BIMA.utils import form_error_redirect, form_success_redirect, is_admin_ormawa, is_admin_kemahasiswaan

from ormawa.forms import *
from ormawa.models import Ormawa, Kepengurusan, Periode
from mahasiswa.models import Mahasiswa


class KepengurusanView:
        
    def index(request):

        form = KepengurusanForm()

        if is_admin_kemahasiswaan(request.user):
            kepengurusan = Kepengurusan.objects.all()
        elif is_admin_ormawa(request.user):
            usrKepengurusan = Kepengurusan.objects.filter(Q(ketua__nim=request.user) | Q(sekretaris__nim=request.user)).first()
            kepengurusan = Kepengurusan.objects.filter(ormawa=usrKepengurusan.ormawa)
        else:
            kepengurusan = []

        context = {
            'kepengurusan': kepengurusan,
            'form': form,
        }
        return render(request, 'kepengurusan/index.html', context)

    def show(request, ormawaName, periodeID):
        form = PengurusForm()
        allowModify = True

        if is_admin_kemahasiswaan(request.user):
            kepengurusan = Kepengurusan.objects.get(ormawa__singkatan=ormawaName, periode__id=periodeID)
            pengurus = Pengurus.objects.filter(kepengurusan=kepengurusan)
        elif is_admin_ormawa(request.user):
            usrKepengurusan = Kepengurusan.objects.filter(Q(ketua__nim=request.user) | Q(sekretaris__nim=request.user)).first()
            kepengurusan = Kepengurusan.objects.get(ormawa=usrKepengurusan.ormawa, periode__id=periodeID)
            pengurus = Pengurus.objects.filter(kepengurusan=kepengurusan)
            if usrKepengurusan != kepengurusan:
                allowModify = False
        else:
            kepengurusan = None
            pengurus = []    

        context = {
            "form": form,
            "allowModify": allowModify,
            "pengurus": pengurus,
            "kepengurusan": kepengurusan,
            "isKepengurusan": True,
        }
        return render(request, 'pengurus/index.html', context)


    @permission_required('ormawa.add_kepengurusan')
    def create(request):
        if request.method == "POST":
            form = KepengurusanForm(request.POST)

            if is_admin_kemahasiswaan(request.user):

                if form.is_valid():
                    kepengurusan = form.save(commit=False)
                    kepengurusan.ormawa = Ormawa.objects.get(id=form.cleaned_data['ormawa'])
                    kepengurusan.periode = Periode.objects.get(id=form.cleaned_data['periode'])
                    kepengurusan.ketua = Mahasiswa.objects.get(nim=form.cleaned_data['ketua'])
                    kepengurusan.sekretaris = Mahasiswa.objects.get(nim=form.cleaned_data['sekretaris'])
                    kepengurusan.npp_wr = Dosen.objects.get(npp=form.cleaned_data['npp_wr'])
                    kepengurusan.npp_pembina = Dosen.objects.get(npp=form.cleaned_data['npp_pembina'])
                    kepengurusan.or_koord = Mahasiswa.objects.get(nim=form.cleaned_data['or_koord'])
                    kepengurusan.save()

                    # return redirect('kepengurusan.index')
                    return form_success_redirect(request, message="Success Created Kepengurusan", route='kepengurusan.index')
                else:
                    return form_error_redirect(request, form=form, route='kepengurusan.index')

    @permission_required('ormawa.change_kepengurusan')
    def edit(request, kepengurusanID):
        if request.method == "POST":
            kepengurusanInstance = Kepengurusan.objects.get(id=kepengurusanID)
            form = KepengurusanForm(request.POST, instance=kepengurusanInstance)

            if is_admin_kemahasiswaan(request.user):

                if form.is_valid():
                    kepengurusan = form.save(commit=False)
                    kepengurusan.ormawa = Ormawa.objects.get(id=form.cleaned_data['ormawa'])
                    kepengurusan.periode = Periode.objects.get(id=form.cleaned_data['periode'])
                    kepengurusan.ketua = Mahasiswa.objects.get(nim=form.cleaned_data['ketua'])
                    kepengurusan.sekretaris = Mahasiswa.objects.get(nim=form.cleaned_data['sekretaris'])
                    kepengurusan.npp_wr = Dosen.objects.get(npp=form.cleaned_data['npp_wr'])
                    kepengurusan.npp_pembina = Dosen.objects.get(npp=form.cleaned_data['npp_pembina'])
                    kepengurusan.or_koord = Mahasiswa.objects.get(nim=form.cleaned_data['or_koord'])
                    kepengurusan.save()
                    # return redirect('kepengurusan.index')
                    return form_success_redirect(request, message="Success Modified Kepengurusan", route='kepengurusan.index')
                else:
                    return form_error_redirect(request, form=form, route='kepengurusan.index')

        else:
            data = {
                "id": kepengurusan.id,
                "nama": kepengurusan.nama,
                "ketua": kepengurusan.ketua.id,
                "sekretaris": kepengurusan.sekretaris.id,
                "tgl_mulai": kepengurusan.periode.tgl_mulai,
                "tgl_selesai": kepengurusan.periode.tgl_selesai,
            }
            return JsonResponse(data, safe=False) 
    
    @permission_required('ormawa.delete_kepengurusan')
    def delete(request, kepengurusanID):
        if request.method == "POST":

            if is_admin_kemahasiswaan(request.user):
                kepengurusan = Kepengurusan.objects.filter(id=kepengurusanID).first()
                kepengurusan.delete()
                # return redirect('kepengurusan.index')
                
                return form_success_redirect(request, message="Success Deleted Kepengurusan", route='kepengurusan.index')
