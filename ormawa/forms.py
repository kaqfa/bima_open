from django import forms
from .models import *


class KepengurusanForm(forms.ModelForm):
    class Meta:
        model = Kepengurusan
        fields = [
            'nama', 
            'jabatan_koord',
        ]
    def __init__(self, *args, **kwargs):
        super(KepengurusanForm, self).__init__(*args, **kwargs)
        empty_choice = [('', 'Pilih Periode')]
        self.fields['periode'].choices = empty_choice + list(Periode.objects.all().values_list('id', 'nama'))

        empty_choice = [('', 'Pilih Ormawa')]
        self.fields['ormawa'].choices = empty_choice + list(Ormawa.objects.all().values_list('id', 'nama'))

    nama = forms.CharField(
        label="Nama Kepengurusan",
        widget=forms.TextInput(
            attrs={
                'x-model': "kepengurusanEdit.nama",
                'placeholder':'Masukan Nama Kepengurusan',
                'class':'w-full rounded-lg border-[1.5px] border-stroke bg-transparent px-5 py-3 font-normal text-black outline-none transition focus:border-primary active:border-primary disabled:cursor-default disabled:bg-whiter dark:border-form-strokedark dark:bg-form-input dark:text-white dark:focus:border-primary',
            },
        ),
        error_messages = { 
            'required':"Field Nama Kepengurusan Tidak Valid"
        }
    )
    ormawa = forms.ChoiceField(
        label="Nama Ormawa",
        widget=forms.Select(
            attrs={
                'x-model': "kepengurusanEdit.ormawa",
                'placeholder':'Masukan Nama Organisasi',
                'class':'w-full rounded-lg border-[1.5px] border-stroke bg-transparent px-5 py-3 font-normal text-black outline-none transition focus:border-primary active:border-primary disabled:cursor-default disabled:bg-whiter dark:border-form-strokedark dark:bg-form-input dark:text-white dark:focus:border-primary',
            },
        ),
        error_messages = { 
            'required':"Field Nama Ormawa Tidak Valid"
        },
    )
    periode = forms.ChoiceField(
        label="Periode", 
        widget=forms.Select(
            attrs={
                'x-model': "kepengurusanEdit.periode",
                'class': 'w-full rounded-lg border-[1.5px] border-stroke bg-transparent px-5 py-3 font-normal text-black outline-none transition focus:border-primary active:border-primary disabled:cursor-default disabled:bg-whiter dark:border-form-strokedark dark:bg-form-input dark:text-white dark:focus:border-primary',
            },
        ),
        error_messages = { 
            'required':"Field Periode Tidak Valid"
        },
    )
    ketua = forms.CharField(
        label="Ketua",
        widget=forms.TextInput(
            attrs={
                'x-model': "kepengurusanEdit.ketua",
                '@input': '''liveSearch('mahasiswa', 'ketua')''',
                'placeholder':'Masukan NIM Mahasiswa',
                'class':'w-full rounded-lg border-[1.5px] border-stroke bg-transparent px-5 py-3 font-normal text-black outline-none transition focus:border-primary active:border-primary disabled:cursor-default disabled:bg-whiter dark:border-form-strokedark dark:bg-form-input dark:text-white dark:focus:border-primary',
            },
        ),
        error_messages = { 
            'required':"Field Ketua Tidak Valid"
        },
    )
    sekretaris = forms.CharField(
        label="Sekretaris",
        widget=forms.TextInput(
            attrs={
                'x-model': "kepengurusanEdit.sekretaris",
                '@input': '''liveSearch('mahasiswa', 'sekretaris')''',
                'placeholder':'Masukan NIM Mahasiswa',
                'class':'w-full rounded-lg border-[1.5px] border-stroke bg-transparent px-5 py-3 font-normal text-black outline-none transition focus:border-primary active:border-primary disabled:cursor-default disabled:bg-whiter dark:border-form-strokedark dark:bg-form-input dark:text-white dark:focus:border-primary',
            },
            
        ),
        error_messages = { 
            'required':"Field Sekretaris Tidak Valid"
        },
    )
    npp_wr = forms.CharField(
        label="NPP WR",
        widget=forms.TextInput(
            attrs={
                'x-model': "kepengurusanEdit.npp_wr",
                '@input': '''liveSearch('dosen', 'npp_wr')''',
                'placeholder':'Masukan NIM Mahasiswa',
                'class':'w-full rounded-lg border-[1.5px] border-stroke bg-transparent px-5 py-3 font-normal text-black outline-none transition focus:border-primary active:border-primary disabled:cursor-default disabled:bg-whiter dark:border-form-strokedark dark:bg-form-input dark:text-white dark:focus:border-primary',
            },
        ),
        error_messages = { 
            'required':"Field NPP WR Tidak Valid"
        },
    )
    npp_pembina = forms.CharField(
        label="NPP Pembina",
        widget=forms.TextInput(
            attrs={
                'x-model': "kepengurusanEdit.npp_pembina",
                '@input': '''liveSearch('dosen', 'npp_pembina')''',
                'placeholder':'Masukan NIM Mahasiswa',
                'class':'w-full rounded-lg border-[1.5px] border-stroke bg-transparent px-5 py-3 font-normal text-black outline-none transition focus:border-primary active:border-primary disabled:cursor-default disabled:bg-whiter dark:border-form-strokedark dark:bg-form-input dark:text-white dark:focus:border-primary',
            },
        ),
        error_messages = { 
            'required':"Field NPP Pembina Tidak Valid"
        },
    )
    or_koord = forms.CharField(
        label="NIM Ormawa Koord",
        widget=forms.TextInput(
            attrs={
                'x-model': "kepengurusanEdit.or_koord",
                '@input': '''liveSearch('mahasiswa', 'or_koord')''',
                'placeholder':'Masukan NIM Koord',
                'class':'w-full rounded-lg border-[1.5px] border-stroke bg-transparent px-5 py-3 font-normal text-black outline-none transition focus:border-primary active:border-primary disabled:cursor-default disabled:bg-whiter dark:border-form-strokedark dark:bg-form-input dark:text-white dark:focus:border-primary',
            },
            
        ),
        error_messages = { 
            'required':"Field NIM Ormawa Koord Tidak Valid"
        },
    )
    jabatan_koord = forms.CharField(
        label="Jabatan Koord",
        widget=forms.TextInput(
            attrs={
                'x-model': "kepengurusanEdit.jabatan_koord",
                'placeholder':'Masukan Nama Jabatan',
                'class':'w-full rounded-lg border-[1.5px] border-stroke bg-transparent px-5 py-3 font-normal text-black outline-none transition focus:border-primary active:border-primary disabled:cursor-default disabled:bg-whiter dark:border-form-strokedark dark:bg-form-input dark:text-white dark:focus:border-primary',
            },
        ),
        error_messages = { 
            'required':"Field Jabatan Koord Tidak Valid"
        },
    )
 
class PengurusForm(forms.ModelForm):
    class Meta:     
        model = Pengurus
        fields = ['struktur']
        labels = {
            'struktur': 'Jabatan',
        }
        widgets = {
            'struktur': forms.Select(
                attrs={
                    'x-model': "pengurusEdit.idStruktur",
                    'class':'w-full rounded-lg border-[1.5px] border-stroke bg-transparent px-5 py-3 font-normal text-black outline-none transition focus:border-primary active:border-primary disabled:cursor-default disabled:bg-whiter dark:border-form-strokedark dark:bg-form-input dark:text-white dark:focus:border-primary',
                },
            ),
        }
    mahasiswa = forms.CharField(
        label="Nama",
        widget=forms.TextInput(
           attrs={
                    'x-model': "pengurusEdit.mahasiswa",
                    '@input': '''liveSearch('mahasiswa', 'mahasiswa')''',
                    'class':'w-full rounded-lg border-[1.5px] border-stroke bg-transparent px-5 py-3 font-normal text-black outline-none transition focus:border-primary active:border-primary disabled:cursor-default disabled:bg-whiter dark:border-form-strokedark dark:bg-form-input dark:text-white dark:focus:border-primary',
            },
        )
    )    

class StrukturOrgForm(forms.ModelForm):
    class Meta:
        model = StrukturOrg
        fields = ['nama']
        labels = {
            'nama': 'Nama Jabatan',
        }
        widgets = {
            'nama': forms.TextInput(
                attrs={
                  'x-model':'namaJabatan',
                  'placeholder':'Nama Jabatan',
                  'class':'w-full rounded-lg border-[1.5px] border-stroke bg-transparent px-5 py-3 font-normal text-black outline-none transition focus:border-primary active:border-primary disabled:cursor-default disabled:bg-whiter dark:border-form-strokedark dark:bg-form-input dark:text-white dark:focus:border-primary',
                },
            ),
        }

class KegiatanForm(forms.ModelForm):

    class Meta:
        model = Kegiatan
        fields = [
            'nomor',
            'nama', 
            'tema',
            'tgl_mulai', 
            'tgl_selesai', 
            'tempat',
            'max_peserta',
            'rencana_anggaran', 
        ]
    nomor = forms.CharField(
        label="Nomor",
        widget=forms.TextInput(
            attrs={
                'x-model':'kegiatanEdit.nomor',
                'placeholder':'Nomor',
                'class':'w-full rounded-lg border-[1.5px] border-stroke bg-transparent px-5 py-3 font-normal text-black outline-none transition focus:border-primary active:border-primary disabled:cursor-default disabled:bg-whiter dark:border-form-strokedark dark:bg-form-input dark:text-white dark:focus:border-primary',
            }
        )
    )
    nama = forms.CharField(
        label="Nama",
        widget=forms.TextInput(
            attrs={
                'x-model':'kegiatanEdit.nama',
                'placeholder':'Nama Kegiatan',
                'class':'w-full rounded-lg border-[1.5px] border-stroke bg-transparent px-5 py-3 font-normal text-black outline-none transition focus:border-primary active:border-primary disabled:cursor-default disabled:bg-whiter dark:border-form-strokedark dark:bg-form-input dark:text-white dark:focus:border-primary',
            }
        )
    )    
    tema = forms.CharField(
        label="Tema",
        widget=forms.TextInput(
            attrs={
                'x-model':'kegiatanEdit.tema',
                'placeholder':'Nama Tema',
                'class':'w-full rounded-lg border-[1.5px] border-stroke bg-transparent px-5 py-3 font-normal text-black outline-none transition focus:border-primary active:border-primary disabled:cursor-default disabled:bg-whiter dark:border-form-strokedark dark:bg-form-input dark:text-white dark:focus:border-primary',
            }
        )
    )    
    tgl_mulai = forms.DateField(
        label="Tanggal Mulai",
        widget=forms.DateInput(
            attrs={
                'x-model':'kegiatanEdit.tgl_mulai',
                'placeholder':'',
                'class':'w-full rounded-lg border-[1.5px] border-stroke bg-transparent px-5 py-3 font-normal text-black outline-none transition focus:border-primary active:border-primary disabled:cursor-default disabled:bg-whiter dark:border-form-strokedark dark:bg-form-input dark:text-white dark:focus:border-primary',
                'type': 'date',
            }
        )
    )
    tgl_selesai = forms.DateField(
        label="Tanggal Selesai",
        widget=forms.DateInput(
            attrs={
                'x-model':'kegiatanEdit.tgl_selesai',
                'class':'w-full rounded-lg border-[1.5px] border-stroke bg-transparent px-5 py-3 font-normal text-black outline-none transition focus:border-primary active:border-primary disabled:cursor-default disabled:bg-whiter dark:border-form-strokedark dark:bg-form-input dark:text-white dark:focus:border-primary',
                'type': 'date',
            }
        )
    )
    mhs_pj = forms.CharField(
        label="Mahasiswa Penanggung Jawab",
        widget=forms.TextInput(
            attrs={
                'placeholder': 'XXX.XXXX.XXXXX',
                '@input': '''liveSearch('mahasiswa', 'mhs_pj')''',
                'x-model':'kegiatanEdit.mhs_pj',
                'class':'w-full rounded-lg border-[1.5px] border-stroke bg-transparent px-5 py-3 font-normal text-black outline-none transition focus:border-primary active:border-primary disabled:cursor-default disabled:bg-whiter dark:border-form-strokedark dark:bg-form-input dark:text-white dark:focus:border-primary',
            }
        )
    )
    mhs_koord = forms.CharField(
        label="Mahasiswa Koordinator",
        widget=forms.TextInput(
            attrs={
                'placeholder': 'XXX.XXXX.XXXXX',
                '@input': '''liveSearch('mahasiswa', 'mhs_koord')''',
                'x-model':'kegiatanEdit.mhs_koord',
                'class':'w-full rounded-lg border-[1.5px] border-stroke bg-transparent px-5 py-3 font-normal text-black outline-none transition focus:border-primary active:border-primary disabled:cursor-default disabled:bg-whiter dark:border-form-strokedark dark:bg-form-input dark:text-white dark:focus:border-primary',
            }
        )
    )
    tempat = forms.CharField(
        label="Tempat",
         widget=forms.TextInput(
            attrs={
                'x-model':'kegiatanEdit.tempat',
                'class':'w-full rounded-lg border-[1.5px] border-stroke bg-transparent px-5 py-3 font-normal text-black outline-none transition focus:border-primary active:border-primary disabled:cursor-default disabled:bg-whiter dark:border-form-strokedark dark:bg-form-input dark:text-white dark:focus:border-primary',
            }
         )
    )
    max_peserta = forms.CharField(
        label="Maksimal Peserta",
         widget=forms.TextInput(
            attrs={
                'x-model':'kegiatanEdit.max_peserta',
                'class':'w-full rounded-lg border-[1.5px] border-stroke bg-transparent px-5 py-3 font-normal text-black outline-none transition focus:border-primary active:border-primary disabled:cursor-default disabled:bg-whiter dark:border-form-strokedark dark:bg-form-input dark:text-white dark:focus:border-primary',
            }
         )
    )
    rencana_anggaran = forms.CharField(
        label="Rencana Anggaran",
         widget=forms.TextInput(
            attrs={
                'x-model':'kegiatanEdit.rencana_anggaran',
                'class':'w-full rounded-lg border-[1.5px] border-stroke bg-transparent px-5 py-3 font-normal text-black outline-none transition focus:border-primary active:border-primary disabled:cursor-default disabled:bg-whiter dark:border-form-strokedark dark:bg-form-input dark:text-white dark:focus:border-primary',
            }
         )
    )
    
class KegiatanDKForm(forms.ModelForm):

    class Meta:
        model = Kegiatan
        fields = [
            'nomor',
            'nama', 
            'tema',
            'tgl_mulai', 
            'tgl_selesai', 
            'tempat',
            'max_peserta',
            'rencana_anggaran', 
        ]
    ormawa = forms.CharField(
        label="Nama Ormawa",
        widget=forms.TextInput(
            attrs={
                'x-model':'kegiatanEdit.ormawa',
                '@input': '''ormawaLiveSearch('ormawa', 'ormawa')''',
                'placeholder':'Cari Ormawa',
                'class':'w-full rounded-lg border-[1.5px] border-stroke bg-transparent px-5 py-3 font-normal text-black outline-none transition focus:border-primary active:border-primary disabled:cursor-default disabled:bg-whiter dark:border-form-strokedark dark:bg-form-input dark:text-white dark:focus:border-primary',
            }
        ) 
    )
    nomor = forms.CharField(
        label="Nomor",
        widget=forms.TextInput(
            attrs={
                'x-model':'kegiatanEdit.nomor',
                'placeholder':'Nomor',
                'class':'w-full rounded-lg border-[1.5px] border-stroke bg-transparent px-5 py-3 font-normal text-black outline-none transition focus:border-primary active:border-primary disabled:cursor-default disabled:bg-whiter dark:border-form-strokedark dark:bg-form-input dark:text-white dark:focus:border-primary',
            }
        )
    )
    nama = forms.CharField(
        label="Nama",
        widget=forms.TextInput(
            attrs={
                'x-model':'kegiatanEdit.nama',
                'placeholder':'Nama Kegiatan',
                'class':'w-full rounded-lg border-[1.5px] border-stroke bg-transparent px-5 py-3 font-normal text-black outline-none transition focus:border-primary active:border-primary disabled:cursor-default disabled:bg-whiter dark:border-form-strokedark dark:bg-form-input dark:text-white dark:focus:border-primary',
            }
        )
    )    
    tema = forms.CharField(
        label="Tema",
        widget=forms.TextInput(
            attrs={
                'x-model':'kegiatanEdit.tema',
                'placeholder':'Nama Tema',
                'class':'w-full rounded-lg border-[1.5px] border-stroke bg-transparent px-5 py-3 font-normal text-black outline-none transition focus:border-primary active:border-primary disabled:cursor-default disabled:bg-whiter dark:border-form-strokedark dark:bg-form-input dark:text-white dark:focus:border-primary',
            }
        )
    )    
    tgl_mulai = forms.DateField(
        label="Tanggal Mulai",
        widget=forms.DateInput(
            attrs={
                'x-model':'kegiatanEdit.tgl_mulai',
                'placeholder':'',
                'class':'w-full rounded-lg border-[1.5px] border-stroke bg-transparent px-5 py-3 font-normal text-black outline-none transition focus:border-primary active:border-primary disabled:cursor-default disabled:bg-whiter dark:border-form-strokedark dark:bg-form-input dark:text-white dark:focus:border-primary',
                'type': 'date',
            }
        )
    )
    tgl_selesai = forms.DateField(
        label="Tanggal Selesai",
        widget=forms.DateInput(
            attrs={
                'x-model':'kegiatanEdit.tgl_selesai',
                'class':'w-full rounded-lg border-[1.5px] border-stroke bg-transparent px-5 py-3 font-normal text-black outline-none transition focus:border-primary active:border-primary disabled:cursor-default disabled:bg-whiter dark:border-form-strokedark dark:bg-form-input dark:text-white dark:focus:border-primary',
                'type': 'date',
            }
        )
    )
    mhs_pj = forms.CharField(
        label="Mahasiswa Penanggung Jawab",
        widget=forms.TextInput(
            attrs={
                'placeholder': 'Cari NIM / Nama Mhs',
                '@input': '''liveSearch('mahasiswa', 'mhs_pj')''',
                'x-model':'kegiatanEdit.mhs_pj',
                'class':'w-full rounded-lg border-[1.5px] border-stroke bg-transparent px-5 py-3 font-normal text-black outline-none transition focus:border-primary active:border-primary disabled:cursor-default disabled:bg-whiter dark:border-form-strokedark dark:bg-form-input dark:text-white dark:focus:border-primary',
            }
        )
    )
    mhs_koord = forms.CharField(
        label="Mahasiswa Koordinator",
        widget=forms.TextInput(
            attrs={
                'placeholder': 'Cari NIM / Nama Mhs',
                '@input': '''liveSearch('mahasiswa', 'mhs_koord')''',
                'x-model':'kegiatanEdit.mhs_koord',
                'class':'w-full rounded-lg border-[1.5px] border-stroke bg-transparent px-5 py-3 font-normal text-black outline-none transition focus:border-primary active:border-primary disabled:cursor-default disabled:bg-whiter dark:border-form-strokedark dark:bg-form-input dark:text-white dark:focus:border-primary',
            }
        )
    )
    tempat = forms.CharField(
        label="Tempat",
         widget=forms.TextInput(
            attrs={
                'x-model':'kegiatanEdit.tempat',
                'class':'w-full rounded-lg border-[1.5px] border-stroke bg-transparent px-5 py-3 font-normal text-black outline-none transition focus:border-primary active:border-primary disabled:cursor-default disabled:bg-whiter dark:border-form-strokedark dark:bg-form-input dark:text-white dark:focus:border-primary',
            }
         )
    )
    max_peserta = forms.CharField(
        label="Maksimal Peserta",
         widget=forms.TextInput(
            attrs={
                'x-model':'kegiatanEdit.max_peserta',
                'class':'w-full rounded-lg border-[1.5px] border-stroke bg-transparent px-5 py-3 font-normal text-black outline-none transition focus:border-primary active:border-primary disabled:cursor-default disabled:bg-whiter dark:border-form-strokedark dark:bg-form-input dark:text-white dark:focus:border-primary',
            }
         )
    )
    rencana_anggaran = forms.CharField(
        label="Rencana Anggaran",
         widget=forms.TextInput(
            attrs={
                'x-model':'kegiatanEdit.rencana_anggaran',
                'class':'w-full rounded-lg border-[1.5px] border-stroke bg-transparent px-5 py-3 font-normal text-black outline-none transition focus:border-primary active:border-primary disabled:cursor-default disabled:bg-whiter dark:border-form-strokedark dark:bg-form-input dark:text-white dark:focus:border-primary',
            }
         )
    )

class EditOrmawaForm(forms.ModelForm):
    class Meta:
        model = Ormawa
        fields = [
            'foto', 
            'nama', 
            'singkatan',
            'deskripsi',
            'sejarah', 
            'visi', 
            'misi', 
            'alamat',
            'website', 
            'media_sosial', 
            'tele', 
        ]
        widgets = {
            'status': forms.Select(
                attrs={
                    'x-model': 'ormawaEdit.status',
                    'class': 'w-full rounded-md border-[1.5px] border-stroke bg-transparent px-5 py-2 font-normal text-black outline-none transition focus:border-primary active:border-primary disabled:cursor-default disabled:bg-whiter dark:border-form-strokedark dark:bg-form-input dark:text-white dark:focus:border-primary',
                }
            ),
            'level': forms.Select(
                attrs={
                    'x-model': 'ormawaEdit.level',
                    'class': 'w-full rounded-md border-[1.5px] border-stroke bg-transparent px-5 py-2 font-normal text-black outline-none transition focus:border-primary active:border-primary disabled:cursor-default disabled:bg-whiter dark:border-form-strokedark dark:bg-form-input dark:text-white dark:focus:border-primary',
                }
            ),
        }
    nama = forms.CharField(
        label="Nama",
        widget=forms.TextInput(
            attrs={
                'x-model': 'ormawaEdit.nama',
                'class': 'w-full rounded-md border-[1.5px] border-stroke bg-transparent px-5 py-2 font-normal text-black outline-none transition focus:border-primary active:border-primary disabled:cursor-default disabled:bg-whiter dark:border-form-strokedark dark:bg-form-input dark:text-white dark:focus:border-primary',
            }
        )
    )
    singkatan = forms.CharField(
        label="Singkatan",
        widget=forms.TextInput(
            attrs={
                'x-model': 'ormawaEdit.singkatan',
                'class': 'w-full rounded-md border-[1.5px] border-stroke bg-transparent px-5 py-2 font-normal text-black outline-none transition focus:border-primary active:border-primary disabled:cursor-default disabled:bg-whiter dark:border-form-strokedark dark:bg-form-input dark:text-white dark:focus:border-primary',
            }
        )
    )
    deskripsi = forms.CharField(
        label="Deskripsi",
        widget=forms.Textarea(
            attrs={
                'x-model': 'ormawaEdit.deskripsi',
                'rows': '3', 
                'class': 'w-full rounded-md border-[1.5px] border-stroke bg-transparent px-5 py-2 font-normal text-black outline-none transition focus:border-primary active:border-primary disabled:cursor-default disabled:bg-whiter dark:border-form-strokedark dark:bg-form-input dark:text-white dark:focus:border-primary',
            }
        )
    )
    sejarah = forms.CharField(
        label="Sejarah (Optional)",
        widget=forms.Textarea(
            attrs={
                'x-model': 'ormawaEdit.sejarah',
                'rows': '3', 
                'class': 'w-full rounded-md border-[1.5px] border-stroke bg-transparent px-5 py-2 font-normal text-black outline-none transition focus:border-primary active:border-primary disabled:cursor-default disabled:bg-whiter dark:border-form-strokedark dark:bg-form-input dark:text-white dark:focus:border-primary',
            }
        ),
        required=False,
    )
    visi = forms.CharField(
        label="Visi (Optional)",
        widget=forms.Textarea(
            attrs={
                'x-model': 'ormawaEdit.visi',
                'rows': '3', 
                'class': 'w-full rounded-md border-[1.5px] border-stroke bg-transparent px-5 py-2 font-normal text-black outline-none transition focus:border-primary active:border-primary disabled:cursor-default disabled:bg-whiter dark:border-form-strokedark dark:bg-form-input dark:text-white dark:focus:border-primary',
            }
        ),
        required=False,
    )
    misi = forms.CharField(
        label="Misi (Optional)",
        widget=forms.Textarea(
            attrs={
                'x-model': 'ormawaEdit.misi',
                'rows': '3', 
                'class': 'w-full rounded-md border-[1.5px] border-stroke bg-transparent px-5 py-2 font-normal text-black outline-none transition focus:border-primary active:border-primary disabled:cursor-default disabled:bg-whiter dark:border-form-strokedark dark:bg-form-input dark:text-white dark:focus:border-primary',
            }
        ),
        required=False,
    )
    alamat = forms.CharField(
        label="Alamat (Optional)",
        widget=forms.TextInput(
            attrs={
                'x-model': 'ormawaEdit.alamat',
                'class': 'w-full rounded-md border-[1.5px] border-stroke bg-transparent px-5 py-2 font-normal text-black outline-none transition focus:border-primary active:border-primary disabled:cursor-default disabled:bg-whiter dark:border-form-strokedark dark:bg-form-input dark:text-white dark:focus:border-primary',
            }
        ),
        required=False,
    )
    foto = forms.ImageField(
        label="Logo (Optional)",
        widget=forms.FileInput(
            attrs={
                'x-model': 'ormawaEdit.foto',
                'class': 'w-full rounded-md border-[1.5px] border-stroke bg-transparent px-5 py-2 font-normal text-black outline-none transition focus:border-primary active:border-primary disabled:cursor-default disabled:bg-whiter dark:border-form-strokedark dark:bg-form-input dark:text-white dark:focus:border-primary',
            }
        ),
        required=False,
    )
    website = forms.CharField(
        label="Website (Optional)",
        widget=forms.TextInput(
            attrs={
                'x-model': 'ormawaEdit.website',
                'class': 'w-full rounded-md border-[1.5px] border-stroke bg-transparent px-5 py-2 font-normal text-black outline-none transition focus:border-primary active:border-primary disabled:cursor-default disabled:bg-whiter dark:border-form-strokedark dark:bg-form-input dark:text-white dark:focus:border-primary',
            }
        ),
        required=False,
    )
    media_sosial = forms.CharField(
        label="Media Sosial (Optional)",
        widget=forms.TextInput(
            attrs={
                'x-model': 'ormawaEdit.media_sosial',
                'class': 'w-full rounded-md border-[1.5px] border-stroke bg-transparent px-5 py-2 font-normal text-black outline-none transition focus:border-primary active:border-primary disabled:cursor-default disabled:bg-whiter dark:border-form-strokedark dark:bg-form-input dark:text-white dark:focus:border-primary',
            }
        ),
        required=False,
    )
    tele = forms.CharField(
        label="Telegram (Optional)",
        widget=forms.TextInput(
            attrs={
                'x-model': 'ormawaEdit.tele',
                'class': 'w-full rounded-md border-[1.5px] border-stroke bg-transparent px-5 py-2 font-normal text-black outline-none transition focus:border-primary active:border-primary disabled:cursor-default disabled:bg-whiter dark:border-form-strokedark dark:bg-form-input dark:text-white dark:focus:border-primary',
            }
        ),
        required=False,
    )