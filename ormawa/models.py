from django.db import models
from dosen.models import Dosen
from mahasiswa.models import Mahasiswa
from masterData.models import Ormawa, Periode, Level

class StrukturOrg(models.Model):
    id = models.AutoField(primary_key=True)
    nama = models.CharField(max_length=100)
    ormawa = models.ForeignKey(Ormawa, on_delete=models.CASCADE)
    active = models.BooleanField()
    
    class Meta:
        verbose_name_plural = "Struktur Organisasi"

    def __str__(self):
        return f'{self.nama}'
    
class Kepengurusan(models.Model):
    id = models.AutoField(primary_key=True)
    nama = models.CharField(max_length=100)
    ormawa = models.ForeignKey(Ormawa, related_name='kepengurusan_ormawa', on_delete=models.CASCADE)
    periode = models.ForeignKey(Periode, on_delete=models.CASCADE)
    ketua = models.ForeignKey(Mahasiswa, related_name='kepengurusan_ketua', on_delete=models.CASCADE)
    sekretaris = models.ForeignKey(Mahasiswa, related_name='kepengurusan_sekretaris', on_delete=models.CASCADE)
    npp_wr = models.ForeignKey(Dosen, related_name='kepengurusan_wr', on_delete=models.CASCADE)
    npp_pembina = models.ForeignKey(Dosen, related_name='kepengurusan_pembina', on_delete=models.CASCADE)
    or_koord = models.ForeignKey(Mahasiswa, related_name='kepengurusan_or_koord', on_delete=models.CASCADE)
    jabatan_koord = models.CharField(max_length=40)
    
    class Meta:
        verbose_name_plural = "Kepengurusan"
    
    def __str__(self):
        return f'{self.nama} - {self.ormawa} - {self.periode}'

class Pengurus(models.Model):
    id = models.AutoField(primary_key=True)
    mahasiswa = models.ForeignKey(Mahasiswa, on_delete=models.CASCADE)
    struktur = models.ForeignKey(StrukturOrg, on_delete=models.CASCADE)
    
    kepengurusan = models.ForeignKey(Kepengurusan, on_delete=models.CASCADE)
    
    class Meta:
        verbose_name_plural = "Pengurus"

class StatusKegiatan(models.TextChoices):
    USULAN = 'Usulan'
    USULAN_VALID = 'Usulan Valid'
    PROPOSAL = 'Proposal'
    PROPOSAL_VALID = 'Proposal Valid'
    LPJ = 'LPJ'
    LPJ_VALID = 'LPJ Valid'
    REJECTED = 'Rejected'


class JenisKegiatan(models.Model):
    id = models.AutoField(primary_key=True)
    nama_jenis = models.CharField(max_length=100)

    class Meta:
        verbose_name_plural = "Jenis Kegiatan"

    def __str__(self):
        return self.nama_jenis

class Kegiatan(models.Model):
    id = models.AutoField(primary_key=True)
    nomor = models.CharField(max_length=100)
    nama = models.CharField(max_length=100)
    tema = models.CharField(max_length=200)
    tgl_mulai = models.DateField()
    tgl_selesai = models.DateField()
    mhs_pj = models.ForeignKey(Mahasiswa, related_name='kegiatan_pj', on_delete=models.CASCADE)
    mhs_koord = models.ForeignKey(Mahasiswa, related_name='kegiatan_koord', on_delete=models.CASCADE)
    tempat = models.CharField(max_length=200)
    max_peserta = models.IntegerField()
    rencana_anggaran = models.IntegerField()    
    dana_akademik = models.IntegerField(default=0)
    dana_tambahan = models.IntegerField(default=0)
    real_dana_akademik = models.IntegerField(null=True)
    real_dana_tambahan = models.IntegerField(null=True)

    deskripsi = models.TextField(default='')
    maksud_tujuan = models.TextField(default='')
    latar_belakang = models.TextField(default='')
    dasar = models.TextField(default='')
    penutup = models.TextField(default='')

    hasil = models.TextField(default='')
    
    kepengurusan = models.ForeignKey(Kepengurusan, on_delete=models.CASCADE)
    ormawa = models.ForeignKey(Ormawa, on_delete=models.CASCADE)
    periode = models.ForeignKey(Periode, on_delete=models.CASCADE)
    jenis = models.ForeignKey(JenisKegiatan, on_delete=models.CASCADE)
    status = models.CharField(max_length=20, choices=StatusKegiatan.choices)

    class Meta:
        verbose_name_plural = "Kegiatan"

    def __str__(self):
        return self.nama