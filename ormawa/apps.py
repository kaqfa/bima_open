from django.apps import AppConfig


class OrmawaConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'ormawa'
