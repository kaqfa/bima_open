from django.urls import path
from django.contrib.auth.decorators import login_required
from . import views

# Kepengurusan
urlpatterns = [
    path("kepengurusan", login_required(views.KepengurusanView.index), name="kepengurusan.index"),
    path("kepengurusan/show/<str:ormawaName>/<int:periodeID>", login_required(views.KepengurusanView.show), name="kepengurusan.show"),
    path("kepengurusan/create", login_required(views.KepengurusanView.create), name="kepengurusan.create"),
    path("kepengurusan/edit/<int:kepengurusanID>", login_required(views.KepengurusanView.edit), name="kepengurusan.edit"),
    path("kepengurusan/delete<int:kepengurusanID>", login_required(views.KepengurusanView.delete), name="kepengurusan.delete"),
]

urlpatterns += [
    path("profile-ormawa", login_required(views.ProfileOrmawaView.index), name="profileOrmawa.index"),
    path("profile-ormawa/edit", login_required(views.ProfileOrmawaView.edit), name="profileOrmawa.edit"),
]
    
# Pengurus
urlpatterns += [
    path("pengurus", login_required(views.PengurusView.index), name="pengurus.index"),
    path("pengurus/create/<int:kepengurusanID>", login_required(views.PengurusView.create), name="pengurus.create"),
    path("pengurus/edit/<int:pengurusID>", login_required(views.PengurusView.edit), name="pengurus.edit"),
    path("pengurus/delete/<int:pengurusID>", login_required(views.PengurusView.delete), name="pengurus.delete"),
]

# Struktur Organisasi
urlpatterns += [
    path("struktur-organisasi", login_required(views.StrukturOrgView.index), name="strukturorg.index"),
    path("struktur-organisasi/create/<int:ormawaID>", login_required(views.StrukturOrgView.create), name="strukturorg.create"),
    path("struktur-organisasi/edit/<int:strukturOrgID>", login_required(views.StrukturOrgView.edit), name="strukturorg.edit"),
    path("struktur-organisasi/delete/<int:strukturOrgID>", login_required(views.StrukturOrgView.delete), name="strukturorg.delete"),
]

# RKT
urlpatterns += [
    path("kegiatan/rkt", login_required(views.RKTView.index), name="kegiatan.index"),
    path("kegiatan/rkt/create", login_required(views.RKTView.create), name="kegiatan.create"),
    path("kegiatan/rkt/edit/<int:kegiatanID>", login_required(views.RKTView.edit), name="kegiatan.edit"),
    path("kegiatan/rkt/delete/<int:kegiatanID>", login_required(views.RKTView.delete), name="kegiatan.delete"),
    path("kegiatan/rkt/verify/<int:kegiatanID>", login_required(views.RKTView.admin_verify), name="kegiatan.verify"),
]

# Duta Kampus
urlpatterns += [
    path("kegiatan/duta-kampus", login_required(views.DutaKampusView.index), name="dutaKampus.index"),
    path("kegiatan/duta-kampus/create", login_required(views.DutaKampusView.create), name="dutaKampus.create"),
    path("kegiatan/duta-kampus/edit/<int:dutaKampusID>", login_required(views.DutaKampusView.edit), name="dutaKampus.edit"),
    path("kegiatan/duta-kampus/delete/<int:dutaKampusID>", login_required(views.DutaKampusView.delete), name="dutaKampus.delete"),
]

