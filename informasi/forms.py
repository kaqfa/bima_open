from django import forms
from .models import Berita, Sambutan, Pengumuman

class BeritaForm(forms.ModelForm):
    class Meta:
        model = Berita
        fields = ['judul', 'content', 'sender', 'createdAt', 'expiredAt']

    judul = forms.CharField(
        label = 'Judul',
        widget = forms.TextInput(
            attrs={
                'x-model': "beritaEdit.judul",
                'class': 'w-full rounded-md border-[1.5px] border-stroke bg-transparent px-5 py-2 font-normal text-black outline-none transition focus:border-primary active:border-primary disabled:cursor-default disabled:bg-whiter dark:border-form-strokedark dark:bg-form-input dark:text-white dark:focus:border-primary',
            }
        )
    )
    content = forms.CharField(
        label = 'Content',
        widget = forms.Textarea(
            attrs={
                'x-model': "beritaEdit.content",
                'rows': '4',
                'class': 'w-full rounded-md border-[1.5px] border-stroke bg-transparent px-5 py-2 font-normal text-black outline-none transition focus:border-primary active:border-primary disabled:cursor-default disabled:bg-whiter dark:border-form-strokedark dark:bg-form-input dark:text-white dark:focus:border-primary',
            }
        )
    )
    sender = forms.CharField(
        label = 'Sender',
        widget = forms.TextInput(
            attrs={
                'x-model': "beritaEdit.sender",
                'class': 'w-full rounded-md border-[1.5px] border-stroke bg-transparent px-5 py-2 font-normal text-black outline-none transition focus:border-primary active:border-primary disabled:cursor-default disabled:bg-whiter dark:border-form-strokedark dark:bg-form-input dark:text-white dark:focus:border-primary',
            }
        )
    )
    createdAt = forms.DateField(
        label = 'Tanggal Posting',
        widget = forms.DateInput(
            attrs={
                'x-model': "beritaEdit.createdAt",
                'class': 'w-full rounded-md border-[1.5px] border-stroke bg-transparent px-5 py-2 font-normal text-black outline-none transition focus:border-primary active:border-primary disabled:cursor-default disabled:bg-whiter dark:border-form-strokedark dark:bg-form-input dark:text-white dark:focus:border-primary',
                'type': 'date',
            },
        ),
    )
    expiredAt = forms.DateField(
        label = 'Tanggal Expired',
        widget = forms.DateInput(
            format='%d%m%Y',
            attrs={
                'x-model': "beritaEdit.expiredAt",
                'class': 'w-full rounded-md border-[1.5px] border-stroke bg-transparent px-5 py-2 font-normal text-black outline-none transition focus:border-primary active:border-primary disabled:cursor-default disabled:bg-whiter dark:border-form-strokedark dark:bg-form-input dark:text-white dark:focus:border-primary',
                'type': 'date',
            }
        ),
        required = False,
    )
    

class PengumumanForm(forms.ModelForm):
    class Meta:
        model = Pengumuman
        fields = ["judul", "content", "sender", "createdAt", "broadcast_mhs", "broadcast_dsn"]
    
    judul = forms.CharField(
        label="Judul",
        widget=forms.TextInput(
            attrs={
                'x-model': "pengumumanEdit.judul",
                'class': 'w-full rounded-md border-[1.5px] border-stroke bg-transparent px-5 py-2 font-normal text-black outline-none transition focus:border-primary active:border-primary disabled:cursor-default disabled:bg-whiter dark:border-form-strokedark dark:bg-form-input dark:text-white dark:focus:border-primary',
            }
        )
    )
    content = forms.CharField(
        label="Isi Pengumuman",
        widget=forms.Textarea(
            attrs={
                'x-model': "pengumumanEdit.content",
                'rows': '3',
                'class': 'w-full rounded-md border-[1.5px] border-stroke bg-transparent px-5 py-2 font-normal text-black outline-none transition focus:border-primary active:border-primary disabled:cursor-default disabled:bg-whiter dark:border-form-strokedark dark:bg-form-input dark:text-white dark:focus:border-primary',
            }
        )
    )
    sender = forms.CharField(
        label="Sender",
        widget=forms.TextInput(
            attrs={
                'x-model': "pengumumanEdit.sender",
                'class': 'w-full rounded-md border-[1.5px] border-stroke bg-transparent px-5 py-2 font-normal text-black outline-none transition focus:border-primary active:border-primary disabled:cursor-default disabled:bg-whiter dark:border-form-strokedark dark:bg-form-input dark:text-white dark:focus:border-primary',
            }
        )
    )
    createdAt = forms.DateField(
        label = 'Tanggal Posting',
        widget = forms.DateInput(
            attrs={
                'x-model': "pengumumanEdit.createdAt",
                'class': 'w-full rounded-md border-[1.5px] border-stroke bg-transparent px-5 py-2 font-normal text-black outline-none transition focus:border-primary active:border-primary disabled:cursor-default disabled:bg-whiter dark:border-form-strokedark dark:bg-form-input dark:text-white dark:focus:border-primary',
                'type': 'date',
            },
        ),
    )
    broadcast_mhs = forms.BooleanField(
        label="Broadcast Email Mahasiswa",
        widget=forms.CheckboxInput(
            attrs={
                'x-model': "pengumumanEdit.broadcast_mhs",
                'value': '1',
                'class': 'w-full rounded-md border-[1.5px] border-stroke bg-transparent px-5 py-2 font-normal text-black outline-none transition focus:border-primary active:border-primary disabled:cursor-default disabled:bg-whiter dark:border-form-strokedark dark:bg-form-input dark:text-white dark:focus:border-primary',
            }
        ),
        required=False
    )
    broadcast_dsn = forms.BooleanField(
        label="Broadcast Email Dosen",
        widget=forms.CheckboxInput(
            attrs={
                'x-model': "pengumumanEdit.broadcast_dsn",
                'value': '1',
                'class': 'w-full rounded-md border-[1.5px] border-stroke bg-transparent px-5 py-2 font-normal text-black outline-none transition focus:border-primary active:border-primary disabled:cursor-default disabled:bg-whiter dark:border-form-strokedark dark:bg-form-input dark:text-white dark:focus:border-primary',
            }
        ),
        required=False
    )


# class SambutanForm(forms.ModelForm):
#     class Meta:
#         model = Sambutan
#         fields = ['judul', 'content', 'sender', 'createdAt', 'expiredAt']
        
