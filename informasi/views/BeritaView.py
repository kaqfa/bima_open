from django.shortcuts import redirect, render
from BIMA.utils import form_error_redirect, form_success_redirect
from informasi.forms import BeritaForm
from informasi.models import Berita
from django.contrib import messages


class BeritaView:
    def index(request):
        context = {
            'form': BeritaForm(),
            'berita': Berita.objects.all(),
        }
        return render(request, 'admin/berita/index.html', context)

    def create(request):
        if request.method == "POST":
            form = BeritaForm(request.POST)
            if form.is_valid():
                form.save()

                return form_success_redirect(request, message="Success Created Berita", route='berita.index')
            else:
                return form_error_redirect(request, form=form, route='berita.index')


    def edit(request, beritaID):
        if request.method == "POST":
            berita = Berita.objects.get(id=beritaID)
            form = BeritaForm(request.POST, instance=berita)
            if form.is_valid():
                form.save()

                return form_success_redirect(request, message="Success Modified Berita", route='berita.index')
            else:
                return form_error_redirect(request, form=form, route='berita.index')

    def delete(request, beritaID):
        if request.method == "POST":
            Berita.objects.get(id=beritaID).delete()

            return form_success_redirect(request, message="Success Deleted Berita", route='berita.index')
