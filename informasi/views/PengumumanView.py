from django.shortcuts import redirect, render
from BIMA.utils import form_error_redirect, form_success_redirect, is_admin_kemahasiswaan
from informasi.forms import PengumumanForm
from informasi.models import Pengumuman
from django.contrib import messages


class PengumumanView:

    def index(request):
        if is_admin_kemahasiswaan(request.user):
            context = {
                "pengumuman": Pengumuman.objects.all(),
                "form": PengumumanForm(),
            }
            return render(request, 'admin/pengumuman/index.html', context)

    def create(request):
        if request.method == "POST":

            if is_admin_kemahasiswaan(request.user):
                form = PengumumanForm(request.POST)
                if form.is_valid():
                    p = form.save(commit=False)
                    p.broadcast_mhs = 1 if form.cleaned_data.get('broadcast_mhs') else 0
                    p.broadcast_dsn = 1 if form.cleaned_data.get('broadcast_dsn') else 0
                    p.save()

                    return form_success_redirect(request, message="Success Created Pengumuman", route='pengumuman.index')
                else:
                    return form_error_redirect(request, form=form, route='pengumuman.index')


    def edit(request, pengumumanID):
        if request.method == "POST":
            if is_admin_kemahasiswaan(request.user):
                pengumuman = Pengumuman.objects.get(id=pengumumanID)
                form = PengumumanForm(request.POST, instance=pengumuman)
                if form.is_valid():
                    form.save()
                    
                    return form_success_redirect(request, message="Success Modified Pengumuman", route='pengumuman.index')
                else:
                    return form_error_redirect(request, form=form, route='pengumuman.index')

    def delete(request, pengumumanID):
        if request.method == "POST":
            if is_admin_kemahasiswaan(request.user):
                Pengumuman.objects.get(id=pengumumanID).delete()
                
                return form_success_redirect(request, message="Success Deleted Pengumuman", route='pengumuman.index')
    