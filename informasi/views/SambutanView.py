from django.shortcuts import redirect, render
from BIMA.utils import is_admin_kemahasiswaan
from informasi.forms import BeritaForm


class SambutanView:

    def index(request):
        None

    def create(request):
        if request.method == "POST":
            if is_admin_kemahasiswaan(request.user):
                form = BeritaForm(request.POST)
                if form.is_valid():
                    form.save()
                return redirect("berita.create")
        else:
            if is_admin_kemahasiswaan(request.user):
                form = BeritaForm()
                return render(request, "create.html", {"form": form})
    
    def show(request):
        None

    def edit(request):
        None

    def delete(request):
        None