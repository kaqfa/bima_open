from django.urls import path
from django.contrib.auth.decorators import login_required
from . import views

urlpatterns = [
    path("berita", login_required(views.BeritaView.index), name="berita.index"),
    path("berita/create", login_required(views.BeritaView.create), name="berita.create"),
    path("berita/edit/<int:beritaID>", login_required(views.BeritaView.edit), name="berita.edit"),
    path("berita/delete/<int:beritaID>", login_required(views.BeritaView.delete), name="berita.delete"),

    path("pengumuman/", login_required(views.PengumumanView.index), name="pengumuman.index"),
    path("pengumuman/create", login_required(views.PengumumanView.create), name="pengumuman.create"),
    path("pengumuman/edit/<int:pengumumanID>", login_required(views.PengumumanView.edit), name="pengumuman.edit"),
    path("pengumuman/delete/<int:pengumumanID>", login_required(views.PengumumanView.delete), name="pengumuman.delete"),

    # path("sambutan/", views.Sambutan.view, name="sambutan.view"),
    # path("sambutan/create", views.Sambutan.create, name="sambutan.create"),
    # path("sambutan/show/{id}", views.Sambutan.show, name="sambutan.show"),
    # path("sambutan/edit/{id}", views.Sambutan.edit, name="sambutan.edit"),
    # path("sambutan/delete/{id}", views.Sambutan.delete, name="sambutan.delete"),

]